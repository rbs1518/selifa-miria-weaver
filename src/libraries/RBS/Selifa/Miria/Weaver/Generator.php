<?php
namespace RBS\Selifa\Miria\Weaver;

/**
 * Class Generator
 * @package RBS\Selifa\Miria\Weaver
 */
class Generator
{
    /**
     * @param array $array
     * @return bool
     */
    private function _isAssociativeArray($array)
    {
        return (array_keys($array) !== range(0, count($array) - 1));
    }

    /**
     * @param string $lines
     * @param array $data
     * @param string $prefix
     */
    protected function _writeArrayToFile(&$lines,$data,$prefix='')
    {
        if ($this->_isAssociativeArray($data))
        {
            $lines .= "array(";
            foreach ($data as $key => $item)
            {
                if (is_array($item))
                {
                    $lines .= ("\n".$prefix."\t".'"'.$key.'"'." => ");
                    $this->_writeArrayToFile($lines,$item,($prefix."\t"));
                    $lines .= ",";
                }
                else
                {
                    if (is_string($item))
                    {
                        $value = str_replace("\\","\\\\",$item);
                        $lines .= ("\n".$prefix."\t".'"'.$key.'" => "'.$value.'"');
                    }
                    else if (is_bool($item))
                        $lines .= ("\n".$prefix."\t".'"'.$key.'" => '.($item?'true':'false'));
                    else
                        $lines .= ("\n".$prefix."\t".'"'.$key.'" => '.$item);
                    $lines .= ",";
                }

            }
            $lines .= "\n".$prefix.")";
        }
        else
        {
            $items = array();
            foreach ($data as $item)
            {
                if (is_string($item))
                {
                    $value = str_replace("\\","\\\\",$item);
                    $items[] = ('"'.$value.'"');
                }
                else if (is_bool($item))
                    $items[] = ($item?'true':'false');
                else
                    $items[] = $item;
            }
            $lines .= "array(".implode(",",$items).")";
        }
    }

    /**
     * @param string $lines
     * @param array $data
     * @param string $prefix
     */
    protected function _writeConfigurationFile(&$lines,$data,$prefix='')
    {
        if ($this->_isAssociativeArray($data))
        {
            $lines .= "array(";
            foreach ($data as $key => $item)
            {
                if (is_array($item))
                {
                    $lines .= ("\n".$prefix."\t".'"'.$key.'"'." => ");
                    $this->_writeConfigurationFile($lines,$item,($prefix."\t"));
                    $lines .= ",";
                }
                else
                {
                    if (is_string($item))
                    {
                        $value = str_replace("\\","\\\\",$item);
                        $lines .= ("\n".$prefix."\t".'"'.$key.'" => "'.$value.'"');
                    }
                    else if (is_bool($item))
                        $lines .= ("\n".$prefix."\t".'"'.$key.'" => '.($item?'true':'false'));
                    else
                        $lines .= ("\n".$prefix."\t".'"'.$key.'" => '.$item);
                    $lines .= ",";
                }

            }
            $lines .= "\n".$prefix.")";
        }
        else
        {
            $items = array();
            foreach ($data as $item)
            {
                if (is_string($item))
                {
                    $value = str_replace("\\","\\\\",$item);
                    $items[] = ('"'.$value.'"');
                }
                else if (is_bool($item))
                    $items[] = ($item?'true':'false');
                else
                    $items[] = $item;
            }
            $lines .= "array(".implode(",",$items).")";
        }
    }

    /**
     * @return string
     */
    public function CreateSelifaInitializationFileContent()
    {
        $s  = "<?php\ndefine('SELIFA_ROOT_PATH',dirname(__FILE__));\n";
        $s .= "define('SELIFA_TIME_STARTED',microtime(true));\n";
        $s .= "define('SELIFA','v1.0');\n";
        $s .= "define('SELIFA_NAME','Selifa');\n\n";
        $s .= "include(SELIFA_ROOT_PATH.'/selifa/core/Core.php');\n?>";
        return $s;
    }

    /**
     * @param string $apnf
     * @param bool $doClose
     * @return string
     */
    public function CreateInitialIndexFileContent($apnf,$doClose=false)
    {
        $s  = "<?php\ninclude('../../initialization.php');\n";
        $s .= "\RBS\Selifa\Core::Initialize(array(\n";
        $s .= "\t'RootPath' => SELIFA_ROOT_PATH,\n";
        $s .= "\t'ConfigDir' => '".$apnf."',\n";
        $s .= "\t'UseComposer' => true,\n";
        $s .= "\t'LoadComponents' => array(\n";
        $s .= "\t\t'\RBS\Selifa\XM'\n";
        $s .= "\t)\n));\n\nuse RBS\Selifa\Miria\Miria;\n\n";
        if ($doClose)
            $s .= "?>";
        return $s;
    }

    /**
     * @param array $data
     * @return string
     */
    public function CreateRouteFileContent($data)
    {
        $lines  = "<?php\n// Generated by Selifa Miria Weaver on ".date('Y-m-d H:i:s').".\n";
        $lines .= "// Any modification to this file will be overwritten by weaver's routing utilities.\n";
        $lines .= "return ";
        $this->_writeArrayToFile($lines,$data,'');
        $lines .= ";\n?>\n";
        return $lines;
    }

    /**
     * @param array $data
     * @return string
     */
    public function CreateConfigurationFile($data)
    {
        $lines  = "<?php\n// Generated by Miria Weaver on ".date('Y-m-d H:i:s').".\n";
        $lines .= "// Any modification to this file will be overwritten by Miria Weaver or Selifa Composer Plugin everytime it run.\n";
        $lines .= "return ";
        $this->_writeConfigurationFile($lines,$data,'');
        $lines .= ";\n?>\n";
        return $lines;
    }
}
?>