<?php
namespace RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver\IO\Console;
use RBS\Selifa\Miria\Weaver\Scaffold\Route;
use RBS\Selifa\Miria\Weaver\IO\Directory;
use RBS\Selifa\Miria\Weaver\IO\File;
use Exception, RBS\Utility\F;
use ZipArchive;

/**
 * Class SelifaApplicationScaffolding
 * @package RBS\Selifa\Miria\Weaver
 */
class SelifaApplicationScaffolding
{
    /**
     * @var null|Weaver
     */
    protected $Weaver = null;

    /**
     * @var string
     */
    protected $AppKey = '';

    /**
     * @var null|array
     */
    protected $AppData = null;

    /**
     * @var null|array
     */
    protected $Data = null;

    /**
     * @var string
     */
    protected $JSONFilePath = '';

    /**
     * @var bool
     */
    protected $DisableSave = false;

    /**
     * @var null|Generator
     */
    protected $Generator = null;

    /**
     * @var null|Route
     */
    public $Route = null;


    public function __construct(Weaver $weaver,$appKey,$disableSave=false)
    {
        $this->AppKey = $appKey;
        $this->DisableSave = $disableSave;
        $this->Weaver = $weaver;

        if (isset($weaver->ComposeData['applications'][$this->AppKey]))
            $this->AppData = $weaver->ComposeData['applications'][$this->AppKey];

        $isExists = $weaver->IsScaffoldingJSONExists('app.'.$appKey);
        if (!$isExists)
        {
            Console::WriteLine('Scaffolding data is not found. Initializing new data.');
            $initialData = array('key' => $appKey);
            $weaver->WriteScaffoldingJSON('app.'.$appKey,$initialData);
            $this->Data = $initialData;
        }
        else
        {
            $this->Data = $weaver->ReadScaffoldingJSON('app.'.$appKey);
        }

        if (!isset($this->Data['routes']))
            $this->Data['routes'] = array();
        $this->Route = new Route($this->Data['routes']);

        $this->Generator = new Generator();
    }

    /**
     *
     */
    public function __destruct()
    {
        $routes = $this->Route->GetRouteDataAsArray();
        $this->Data['routes'] = $routes;

        if (!$this->DisableSave)
        {
            if ($this->AppData == null)
                $this->Weaver->RemoveApplicationData($this->AppKey);
            else
                $this->Weaver->SetApplicationData($this->AppKey,$this->AppData);
            $jsonId = ('app.'.$this->AppKey);
            $this->Weaver->WriteScaffoldingJSON($jsonId,$this->Data);
        }
    }

    /**
     * @return string
     */
    public function GetPath()
    {
        return $this->Weaver->GetSelifaApplicationPath($this->AppKey);
    }

    /**
     * @return string
     */
    public function GetKey()
    {
        return $this->AppKey;
    }

    /**
     * @return string
     */
    public function GetType()
    {
        if (isset($this->AppData['type']))
            return strtolower(trim($this->AppData['type']));
        else
            return '';
    }

    /**
     * @param string $type
     * @return ApplicationGenerator
     * @throws Exception
     */
    public function LoadApplicationGenerator($type='')
    {
        if ($type == '')
            $type = $this->GetType();

        $wcd = $this->Weaver->GetWeaverComposeData();
        $gens = array();
        if (isset($wcd['generators']['application']))
            $gens = $wcd['generators']['application'];

        if (!isset($gens[$type]))
            throw new Exception('Unspecified application generator ('.$type.').');

        $className = $gens[$type];
        $genObject = new $className();
        if (!($genObject instanceof ApplicationGenerator))
            throw new Exception("'".$className."' is not an instance of ApplicationGenerator.");

        $genObject->Initialize($this->Weaver,$this);
        return $genObject;
    }

    /**
     * @param array $appData
     * @param bool $disableWrite
     */
    public function UpdateConfiguration($appData,$disableWrite=false)
    {
        $appPath = ($this->Weaver->GetSelifaApplicationPath($this->AppKey,false));
        $defPaths = F::Extract($this->Weaver->ComposeData,'default-paths',array());

        $configPath = ($appPath.DIRECTORY_SEPARATOR.'configs');
        if (isset($defPaths['config']))
            $configPath = ($appPath.DIRECTORY_SEPARATOR.trim($defPaths['config']));
        if (!file_exists($configPath))
            mkdir($configPath,0777,true);

        $configs = $this->Weaver->GetComponentConfigurations();
        $defaults = F::Extract($this->Weaver->ComposeData,'default-config',array());

        foreach ($configs as $cKey => $options)
        {
            $aOptions = $options;
            if (array_key_exists($cKey,$defaults))
            {
                if ($defaults[$cKey] != null)
                    $aOptions = array_replace_recursive($aOptions,$defaults[$cKey]);
                else
                    $aOptions = null;
            }

            if ($aOptions != null)
            {
                if (array_key_exists($cKey,$appData['config']))
                {
                    if ($appData['config'][$cKey] != null)
                        $aOptions = array_replace_recursive($aOptions,$appData['config'][$cKey]);
                    else
                        $aOptions = null;
                }
            }

            if ($aOptions == null)
                continue;

            if (!$disableWrite)
            {
                $filePath = ($configPath.DIRECTORY_SEPARATOR.$cKey.'.php');
                $fileContent = $this->Generator->CreateConfigurationFile($aOptions);
                File::Put($filePath,$fileContent,true);
                Console::WriteLine("\tCreating configuration {!f(light_cyan)}".$cKey."{/f}");
            }
        }
    }

    /**
     * @param array $aParams
     * @param ApplicationGenerator $appGen
     * @param bool $noConfirm
     * @param bool $forceReinit
     * @return bool
     * @throws Exception
     */
    public function InitializeApplication($aParams,$appGen=null,$noConfirm=false,$forceReinit=false)
    {
        $type = F::Extract($aParams,'Type','default');
        $name = F::Extract($aParams,'Name','DefaultApplication');
        $rNamespace = F::Extract($aParams,'RootNS','WebApplication');

        if (!$noConfirm)
        {
            Console::WriteLine("\nConfigured parameter:");
            foreach ($aParams as $key => $value)
                Console::WriteLine("\t- {!f(yellow)}".$key."{/f}: ".$value);
            $proceed = Console::Confirm("\n".'Are you sure want to initialize new application with above parameters?');
        }
        else
            $proceed = true;

        if ($proceed)
        {
            $izFile = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR.'initialization.php');
            if (!file_exists($izFile))
            {
                $izContent = $this->Generator->CreateSelifaInitializationFileContent();
                file_put_contents($izFile,$izContent);
            }

            $appPath = ($this->Weaver->GetSelifaApplicationPath($this->AppKey,false));
            if (file_exists($appPath))
            {
                if (!$forceReinit)
                {
                    $continue = Console::Confirm("Application '{!f(light_red)}".$this->AppKey."{/f}' is exists. Re-initializing this application will replace or even remove some of the files.\nDo you still want to continue?");
                    if (!$continue)
                    {
                        Console::WriteLine('Application re-initialization {!f(light_red)}cancelled{/f} by user.');
                        return false;
                    }
                }
            }

            Console::WriteLine("Initializing {!f(light_green)}".$type."{/f} application with key '{!f(light_green)}".$this->AppKey."{/f}'...");
            if (!file_exists($appPath))
                mkdir($appPath,0777,true);

            $iData = array(
                'key' => $this->AppKey,
                'type' => $type,
                'name' => $name,
                'namespace' => $rNamespace,
                'paths' => array(
                    'app' => $appPath
                ),
                'config' => array()
            );

            if (isset($this->Weaver->ComposeData['applications'][$this->AppKey]))
                $iData = array_replace_recursive($iData,$this->Weaver->ComposeData['applications'][$this->AppKey]);

            /*$defPaths = F::Extract($this->Weaver->ComposeData,'default-paths',array());
            $configPath = ($appPath.DIRECTORY_SEPARATOR.'configs');
            if (isset($defPaths['config']))
                $configPath = ($appPath.DIRECTORY_SEPARATOR.trim($defPaths['config']));*/

            $defPaths = F::Extract($this->Weaver->ComposeData,'default-paths',array());
            $configPath = 'configs';
            if (isset($defPaths['config']))
                $configPath = trim($defPaths['config']);

            $apnf = ($this->Weaver->GetSelifaApplicationPath($this->AppKey,false,false).DIRECTORY_SEPARATOR.$configPath.DIRECTORY_SEPARATOR);
            $aContents = array(
                'index.php' => $this->Generator->CreateInitialIndexFileContent($apnf,false)
            );

            if ($appGen == null)
                $appGen = $this->LoadApplicationGenerator($type);
            $appGen->Create($this->Weaver,$this,$aParams,$aContents,$iData);

            $aContents['index.php'] .= "\n?>";

            $this->UpdateConfiguration($iData);
            foreach ($aContents as $file => $content)
            {
                $ffp = ($appPath.DIRECTORY_SEPARATOR.$file);
                Console::WriteLine("\tCreating file {!f(light_cyan)}".$file."{/f}");
                File::Put($ffp,$content);
            }

            $this->Route->Add('*','main');
            $this->Route->GenerateFile($appPath);
            $this->AppData = $iData;

            return true;
        }
        else
        {
            Console::WriteLine('Application initialization {!f(light_red)}cancelled{/f} by user.');
            return false;
        }
    }

    /**
     * @param null $appGen
     * @return bool
     */
    public function UpdateApplication($appGen=null)
    {
        if ($appGen == null)
            $appGen = $this->LoadApplicationGenerator($this->GetType());
        $appGen->Update($this->Weaver,$this,'update');

        $data = $this->AppData;
        $this->UpdateConfiguration($data);

        return true;
    }

    /**
     * @param bool $doBackup
     * @param null|ApplicationGenerator $appGen
     */
    public function RemoveApplication($doBackup=true,$appGen=null)
    {
        if ($doBackup)
            $this->BackupApplication();

        if ($appGen == null)
            $appGen = $this->LoadApplicationGenerator($this->GetType());
        $appGen->Remove($this->Weaver,$this);

        $appPath = $this->GetPath();
        Directory::Remove($appPath);

        $this->AppData = null;
        Console::WriteLine("Application '{!f(light_green)}".$this->GetKey()."{/f}' removed.");
    }

    /**
     * @throws Exception
     */
    public function BackupApplication()
    {
        $appPath = $this->GetPath();

        $bfName = ('app.backup.'.$this->GetKey().'.'.date('YmdHi').'.zip');
        $bfp = ($this->Weaver->GetSelifaTemporaryPath().DIRECTORY_SEPARATOR.$bfName);
        if (file_exists($bfp))
            throw new Exception("Backup file '".$bfp."' is exists.");

        $files = array();
        Directory::Enumerate($appPath,'',$files,true);

        $zip = new ZipArchive();
        $zip->open($bfp,ZipArchive::CREATE);
        foreach ($files as $item)
        {
            if ($item['type'] == 'file')
            {
                $fPath = ($appPath.DIRECTORY_SEPARATOR.$item['name']);
                $zip->addFile($fPath,$item['name']);
            }
            else if ($item['type'] == 'dir')
                $zip->addEmptyDir($item['name']);
        }
        $zip->close();
        Console::WriteLine("Application '{!f(light_green)}".$this->GetKey()."{/f}' backed up successfully to file {!f(yellow)}".$bfName."{/f}.");
    }

    /**
     * @return array|mixed
     */
    public function GetRouteData()
    {
        if (isset($this->Data['routes']))
            return $this->Data['routes'];
        else
            return array();
    }
}
?>