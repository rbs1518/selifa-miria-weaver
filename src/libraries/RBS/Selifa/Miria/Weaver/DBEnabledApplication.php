<?php
namespace RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Data\BaseDatabaseDriver;
use RBS\Selifa\Data\DBI;
use RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver\SQL\BaseReflector;
use Exception;

/**
 * Class DBEnabledApplication
 * @package RBS\Selifa\Miria\Weaver
 */
abstract class DBEnabledApplication extends BaseApplication
{
    /**
     * @var null|BaseDatabaseDriver
     */
    protected $DB = null;

    /**
     * @var null|BaseReflector
     */
    protected $Reflector = null;

    /**
     * @var string
     */
    protected $DatabaseId = '';

    /**
     * @var array
     */
    protected $Data = array();

    /**
     * @param Weaver $weaver
     * @param array $options
     * @throws Exception
     */
    public function Initialize(Weaver $weaver, $options)
    {
        if (isset($options['db']))
            $this->DB = DBI::Get(trim($options['db']));
        else
            $this->DB = DBI::Get();

        $this->DatabaseId = $this->DB->DatabaseId;
        $rClass = ($weaver->GetOption('ReflectorDefaultNamespace','')."\\".$this->DB->InterfaceType.$weaver->GetOption('ReflectorClassSuffix',''));

        $rClassFile = ($weaver->GetReflectorPath(false,true).$this->DB->InterfaceType.'.php');
        if (!file_exists($rClassFile))
            throw new Exception("Reflector file ".$rClassFile." does not exists.");
        include($rClassFile);

        $reflectorObject = new $rClass($this->DB);
        $this->Reflector = $reflectorObject;
    }
}
?>