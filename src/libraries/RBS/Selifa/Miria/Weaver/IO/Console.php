<?php
namespace RBS\Selifa\Miria\Weaver\IO;

/**
 * Class Console
 * @package RBS\Selifa\Miria\Weaver\IO
 */
class Console
{
    /**
     * @param string $text
     * @param array $vars
     */
    public static function Write($text,$vars=array())
    {
        $parser = ConsoleFormatParser::Initialize();
        echo $parser->ParseText($text,$vars);
    }

    /**
     * @param string $text
     * @param array $vars
     */
    public static function WriteLine($text,$vars=array())
    {
        $parser = ConsoleFormatParser::Initialize();
        echo ($parser->ParseText($text,$vars)."\n");
    }

    /**
     * @return string
     */
    public static function ReadLine()
    {
        return trim(fgets(STDIN));
    }

    /**
     * @param string $text
     * @param array $vars
     * @param bool $repeatIfEmpty
     * @return string
     */
    public static function AskForValue($text,$vars=array(),$repeatIfEmpty=false)
    {
        if ($repeatIfEmpty)
        {
            $value = '';
            while ($value == '')
            {
                self::Write($text,$vars);
                $value = self::ReadLine();
            }
            return $value;
        }
        else
        {
            self::Write($text,$vars);
            return self::ReadLine();
        }
    }

    /**
     * @param string $text
     * @param string $default
     * @param array $vars
     * @return string
     */
    public static function AskForValueWithDefault($text,$default='',$vars=array())
    {
        self::Write($text.' ['.$default.'] ',$vars);
        $value = self::ReadLine();
        if ($value != '')
            return $value;
        else
            return $default;
    }

    /**
     * @param string $text
     * @param string $default
     * @param array $vars
     * @return bool
     */
    public static function Confirm($text,$default='n',$vars=array())
    {
        self::Write($text.' (yes/no) ['.$default.'] ',$vars);
        $value = self::ReadLine();
        if ($value != '')
            return (substr(strtolower($value),0,1) == 'y');
        else
            return (substr(strtolower($default),0,1) == 'y');
    }
}
?>