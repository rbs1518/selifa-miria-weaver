<?php
namespace RBS\Selifa\Miria\Weaver\IO;
use InvalidArgumentException;

/**
 * Class Directory
 * @package RBS\Selifa\Miria\Weaver\IO
 */
class Directory
{
    /**
     * @param string $baseDir
     * @param string $dir
     * @param array $array
     * @param bool $forZip
     */
    public static function Enumerate($baseDir,$dir,&$array,$forZip=false)
    {
        if ($dir == '')
            $fdirname = $baseDir;
        else
            $fdirname = $baseDir.DIRECTORY_SEPARATOR.$dir;

        $fileCounter = 0;
        $cdir = scandir($fdirname);
        foreach ($cdir as $key => $value)
        {
            if (!in_array($value,array(".","..")))
            {
                $fullPath = ($baseDir.DIRECTORY_SEPARATOR.$dir.DIRECTORY_SEPARATOR.$value);
                if (is_dir($fullPath))
                {
                    if ($dir == '')
                        $nDir = ($value);
                    else
                        $nDir = ($dir.DIRECTORY_SEPARATOR.$value);
                    self::Enumerate($baseDir,$nDir,$array,$forZip);
                }
                else
                {
                    if ($forZip)
                    {
                        if ($dir == '')
                            $array[] = array('type' => 'file', 'name' => $value);
                        else
                            $array[] = array('type' => 'file', 'name' => ($dir.DIRECTORY_SEPARATOR.$value));
                    }
                    else
                    {
                        if ($dir == '')
                            $array[] = $value;
                        else
                            $array[] = ($dir.DIRECTORY_SEPARATOR.$value);
                    }
                    $fileCounter++;
                }
            }
        }

        if (($fileCounter <= 0) && ($forZip))
            $array[] = array('type' => 'dir', 'name' => $dir);
    }

    /**
     * @param string $dir
     */
    public static function Remove($dir)
    {
        if (! is_dir($dir))
            throw new InvalidArgumentException("$dir must be a directory");
        if (substr($dir, strlen($dir) - 1, 1) != DIRECTORY_SEPARATOR)
            $dir .= DIRECTORY_SEPARATOR;
        $files = glob($dir . '*', GLOB_MARK);
        foreach ($files as $file)
        {
            if (is_dir($file))
                self::Remove($file);
            else
                unlink($file);
        }
        rmdir($dir);
    }
}
?>