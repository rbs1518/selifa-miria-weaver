<?php
namespace RBS\Selifa\Miria\Weaver\IO;

/**
 * Interface ICFPObjectRenderer
 * @package RBS\Selifa\Miria\Weaver\IO
 */
interface ICFPObjectRenderer
{
    /**
     * @param ConsoleFormatParser $parser
     * @return mixed
     */
    public function ConsoleRender($parser);
}
?>