<?php
namespace RBS\Selifa\Miria\Weaver\IO;

/**
 * Class BasicFormattingRenderer
 * @package RBS\Selifa\Miria\Weaver\IO
 */
class BasicFormattingRenderer implements ICFPCustomRenderer
{
    /**
     * @param ConsoleFormatParser $parser
     */
    public function RegisterRenderer($parser)
    {
        $parser->RegisterGlobal(array(
            'black' => '0;30',
            'dark_gray' => '1;30',
            'blue' => '0;34',
            'light_blue' => '1;34',
            'green' => '0;32',
            'light_green' => '1;32',
            'cyan' => '0;36',
            'light_cyan' => '1;36',
            'red' => '0;31',
            'light_red' => '1;31',
            'purple' => '0;35',
            'light_purple' => '1;35',
            'brown' => '0;33',
            'yellow' => '1;33',
            'light_gray' => '0;37',
            'white' => '1;37',
            'b_black' => '40',
            'b_red' => '41',
            'b_green' => '42',
            'b_yellow' => '43',
            'b_blue' => '44',
            'b_magenta' => '45',
            'b_cyan' => '46',
            'b_light_gray' => '47',
            'blink' => 5,
            'underline' => 4
        ));
    }

    /**
     * @return array
     */
    public function GetRenderFunctionNames()
    {
        return array('f','v','iw','bws','bwe');
    }

    /**
     * @param string $functionName
     * @param array $parameters
     * @param array $token
     * @param bool $isInverted
     * @param array $vars
     * @return string
     */
    public function Render($functionName, $parameters, $token, $isInverted, $vars)
    {
        if ($functionName == 'f')
        {
            $prefix = '';
            if (count($parameters) <= 0)
                return '';

            foreach ($parameters as $code)
                $prefix .= ("\033[".$code."m");

            $inner = $token['t_end'];
            return ($prefix.$inner."\033[0m");
        }
        else if ($functionName == 'v')
        {
            if (isset($parameters[0]))
                return $parameters[0];
        }
        else if ($functionName == 'iw')
        {
            if (isset($parameters[0]))
                return ('{!'.$parameters[0].'}');
        }
        else if ($functionName == 'bws')
        {
            if (isset($parameters[0]))
                return ('<!--{!'.$parameters[0].'}-->');
        }
        else if ($functionName == 'bwe')
        {
            if (isset($parameters[0]))
                return ('<!--{/'.$parameters[0].'}-->');
        }
        return '';
    }
}
?>