<?php
namespace RBS\Selifa\Miria\Weaver\IO;

/**
 * Class File
 * @package RBS\Selifa\Miria\Weaver\IO
 */
class File
{
    /**
     * @param string $file
     * @param string $data
     * @param bool $overwrite
     */
    public static function Put($file,$data,$overwrite=false)
    {
        $_file = str_replace("\\",'/',$file);
        if (!file_exists($_file))
        {
            $_dir = dirname($_file);
            if (!file_exists($_dir))
                mkdir($_dir,0775,true);
            file_put_contents($_file,$data);
        }
        else
        {
            if ($overwrite)
                file_put_contents($_file,$data);
        }
    }
}
?>