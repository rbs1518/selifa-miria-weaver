<?php
namespace RBS\Selifa\Miria\Weaver\IO;

/**
 * Interface ICFPCustomRenderer
 * @package RBS\Selifa\Miria\Weaver\IO
 */
interface ICFPCustomRenderer
{
    /**
     * @param ConsoleFormatParser $parser
     */
    public function RegisterRenderer($parser);

    /**
     * @return array
     */
    public function GetRenderFunctionNames();

    /**
     * @param string $functionName
     * @param array $parameters
     * @param array $token
     * @param boolean $isInverted
     * @param array $vars
     * @return mixed|null
     */
    public function Render($functionName,$parameters,$token,$isInverted,$vars);
}
?>