<?php
namespace RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver;
use Exception;

/**
 * Class CommandRunner
 * @package RBS\Selifa\Miria\Weaver
 */
abstract class CommandRunner
{
    /**
     * @var string
     */
    protected $Command = '';

    /**
     * @var string
     */
    protected $Function = '';

    /**
     * @var null|Weaver
     */
    protected $Weaver = null;

    /**
     * @var null|BaseApplication
     */
    protected $Application = null;

    /**
     * CommandRunner constructor.
     * @param Weaver $weaver
     * @param BaseApplication $app
     * @param $cmd
     */
    public function __construct(Weaver $weaver, BaseApplication $app, $cmd)
    {
        $this->Weaver = $weaver;
        $this->Application = $app;
        $this->Command = strtolower(trim($cmd));
        $this->Function = str_replace(array('-','.'),'_',$this->Command);

        $this->Initialize();
    }

    /**
     *
     */
    abstract protected function Initialize();

    /**
     * @param array $parameters
     * @return mixed
     * @throws Exception
     */
    public function Execute($parameters)
    {
        if (!method_exists($this,$this->Function))
            throw new Exception('Method '.$this->Function.' does not exists.');
        $result = call_user_func_array(array($this,$this->Function),$parameters);
        return $result;
    }
}
?>