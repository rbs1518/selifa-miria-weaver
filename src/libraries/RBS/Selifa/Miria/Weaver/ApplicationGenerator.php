<?php
namespace RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver\IO\ConsoleFormatParser;
use Exception;

/**
 * Class ApplicationGenerator
 * @package RBS\Selifa\Miria\Weaver
 */
abstract class ApplicationGenerator
{
    /**
     * @var string
     */
    protected $_TemplatePath = '';

    /**
     * @var bool
     */
    protected $_IsUserDefined = false;

    /**
     * @param Weaver $weaver
     * @param SelifaApplicationScaffolding $sas
     */
    public function Initialize(Weaver $weaver, SelifaApplicationScaffolding $sas)
    {
        $aPath = ($weaver->GetTemplatePath($this->_IsUserDefined,true).'app-gen'.DIRECTORY_SEPARATOR.$this->GetType().DIRECTORY_SEPARATOR);
        $this->_TemplatePath = $aPath;
    }

    /**
     * @param string $name
     * @param array $vars
     * @return null|string
     * @throws Exception
     */
    public function ParseTemplate($name,$vars=array())
    {
        $fn = ($this->_TemplatePath.$name.'.ftpl');
        if (!file_exists($fn))
            throw new Exception('Template file '.$name.' does not exists.');

        $content = file_get_contents($fn);
        $parser = ConsoleFormatParser::Initialize();
        return $parser->ParseText($content,$vars);
    }

    /**
     * @return string
     */
    abstract public function GetType();

    /**
     * @param Weaver $weaver
     * @param SelifaApplicationScaffolding $sas
     * @param array $aOptions
     * @param array $aParams
     * @param bool $isQuietMode
     */
    abstract public function GetCustomParameter(Weaver $weaver, SelifaApplicationScaffolding $sas, $aOptions, &$aParams, $isQuietMode=false);

    /**
     * @param Weaver $weaver
     * @param SelifaApplicationScaffolding $sas
     * @param array $aParams
     * @param array $aContent
     * @param array $appData
     */
    abstract public function Create(Weaver $weaver, SelifaApplicationScaffolding $sas, $aParams, &$aContent, &$appData);

    /**
     * @param Weaver $weaver
     * @param SelifaApplicationScaffolding $sas
     * @param string $action
     */
    abstract public function Update(Weaver $weaver, SelifaApplicationScaffolding $sas, $action);

    /**
     * @param Weaver $weaver
     * @param SelifaApplicationScaffolding $sas
     */
    abstract public function Remove(Weaver $weaver, SelifaApplicationScaffolding $sas);
}
?>