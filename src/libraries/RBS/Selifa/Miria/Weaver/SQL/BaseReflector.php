<?php
namespace RBS\Selifa\Miria\Weaver\SQL;
use RBS\Selifa\Data\BaseDatabaseDriver;
use RBS\Utility\Inflector;

/**
 * Class BaseReflector
 * @package RBS\Selifa\Miria\Weaver\SQL
 */
abstract class BaseReflector
{
    /**
     * @var null|BaseDatabaseDriver
     */
    protected $Driver = null;

    /**
     * @var array
     */
    protected $Tables = array();

    /**
     * @var array
     */
    protected $Groups = array();

    /**
     * @var array
     */
    protected $Commits = array();

    /**
     * @var array
     */
    protected $MetaColumns = array(
        'id' => array('skip_log' => true),
        'created' => array('skip_log' => true),
        'modified' => array('skip_log' => false),
        'cuid' => array('skip_log' => true),
        'muid' => array('skip_log' => false)
    );

    /**
     * @param BaseDatabaseDriver $driver
     */
    public function __construct($driver)
    {
        $this->Driver = $driver;
    }

    /**
     * @return array
     */
    public function GetTables()
    {
        return $this->Tables;
    }

    /**
     * @param array $tableSpecs
     */
    public function SetTables($tableSpecs)
    {
        $this->Tables = $tableSpecs;
    }

    /**
     *
     */
    public function ResetAutoIncrementForAllTables()
    {
        $tCount = count($this->Tables);
        for ($i=0;$i<$tCount;$i++)
            $this->Tables[$i]['ai'] = 1;
    }

    /**
     * @param string $groupName
     */
    public function AddGroup($groupName)
    {
        if (!in_array($groupName,$this->Groups))
            $this->Groups[] = $groupName;
    }

    /**
     * @param string $tableName
     * @param string $columnName
     * @param string $groupName
     */
    public function SetColumnGroup($tableName,$columnName,$groupName)
    {
        $tCount = count($this->Tables);
        for ($i=0;$i<$tCount;$i++)
        {
            $item = $this->Tables[$i];
            if (strtolower($item['name']) == strtolower($tableName))
            {
                $rowCount = count($item['rows']);
                for ($j=0;$j<$rowCount;$j++)
                {
                    $row = $item['rows'][$j];
                    if (strtolower($row['field']) == strtolower($columnName))
                    {
                        $this->Tables[$i]['rows'][$j]['group_id'] = true;
                        $this->Tables[$i]['rows'][$j]['group_name'] = $groupName;
                        break;
                    }
                }
            }
        }
    }

    /**
     * @return string
     */
    public function CreateUpdateLogTableSpec()
    {
        $tName = 'UpdateLog';
        $spec = array(
            'name' => $tName,
            'charset' => 'utf8',
            'engine' => 'InnoDB',
            'rows' => array(
                array('field' => 'ID', 'type' => 'BIGINT(20) UNSIGNED', 'nullable' => false, 'extra' => 'AUTO_INCREMENT', 'primary' => true),
                array('field' => 'UID', 'type' => 'INTEGER UNSIGNED', 'nullable' => false, 'default' => 0),
                array('field' => 'DBU', 'type' => 'VARCHAR(32)', 'nullable' => false, 'default' => ''),
                array('field' => 'TableName', 'type' => 'VARCHAR(64)', 'nullable' => false, 'default' => ''),
                array('field' => 'RowID', 'type' => 'BIGINT(20) UNSIGNED', 'nullable' => false, 'default' => 0),
                array('field' => 'EventType', 'type' => 'VARCHAR(16)', 'nullable' => false, 'default' => ''),
                array('field' => 'EventTime', 'type' => 'TIMESTAMP', 'nullable' => false, 'extra' => 'DEFAULT CURRENT_TIMESTAMP'),
                array('field' => 'Data', 'type' => 'MEDIUMTEXT', 'nullable' => false),
                array('field' => 'Remark', 'type' => 'TEXT', 'nullable' => true)
            )
        );
        $this->Tables[$tName] = $spec;
        return $tName;
    }

    /**
     * @param string $groupName
     * @return string
     */
    public function CreateChangeLogTableSpec($groupName)
    {
        $tName = (Inflector::camelize($groupName).'ChangeLog');
        $spec = array(
            'name' => $tName,
            'charset' => 'utf8',
            'engine' => 'InnoDB',
            'rows' => array(
                array('field' => 'ID', 'type' => 'BIGINT(20) UNSIGNED', 'nullable' => false, 'extra' => 'AUTO_INCREMENT', 'primary' => true),
                array('field' => 'UID', 'type' => 'INTEGER UNSIGNED', 'nullable' => false, 'default' => 0),
                array('field' => 'DBU', 'type' => 'VARCHAR(32)', 'nullable' => false, 'default' => ''),
                array('field' => 'TableName', 'type' => 'VARCHAR(64)', 'nullable' => false, 'default' => ''),
                array('field' => 'RowID', 'type' => 'BIGINT(20) UNSIGNED', 'nullable' => false, 'default' => 0),
                array('field' => 'Identifier', 'type' => 'VARCHAR(64)', 'nullable' => false, 'default' => ''),
                array('field' => 'EventType', 'type' => 'VARCHAR(16)', 'nullable' => false, 'default' => ''),
                array('field' => 'EventTime', 'type' => 'TIMESTAMP', 'nullable' => false, 'extra' => 'DEFAULT CURRENT_TIMESTAMP')
            )
        );
        $this->Tables[$tName] = $spec;
        return $tName;
    }

    /**
     * @param BaseDatabaseDriver $driver
     * @param array $ts
     * @return mixed
     */
    abstract protected function _RetrieveTableMeta($driver,&$ts);

    /**
     * @param array $ts
     * @return string
     */
    abstract protected function _CreateTableSyntax($ts);

    /**
     * @param array $ts
     * @param bool $updateLog
     * @param bool $changeLog
     * @return string
     */
    abstract protected function _CreateAfterInsertTrigger($ts,$updateLog=false,$changeLog=false);

    /**
     * @param array $ts
     * @param bool $updateLog
     * @param bool $changeLog
     * @return string
     */
    abstract protected function _CreateAfterUpdateTrigger($ts,$updateLog=false,$changeLog=false);

    /**
     * @param array $ts
     * @param bool $updateLog
     * @param bool $changeLog
     * @return string
     */
    abstract protected function _CreateAfterDeleteTrigger($ts,$updateLog=false,$changeLog=false);

    /**
     * @param bool $doReplace
     */
    public function ReverseEngineerSchema($doReplace=false)
    {
        $eTabs = array();
        $this->_RetrieveTableMeta($this->Driver,$eTabs);

        foreach ($eTabs as $tableItem)
        {
            $tName = strtolower(trim($tableItem['name']));
            if (isset($this->Tables[$tName]))
            {
                if ($doReplace)
                    $this->Tables[$tName] = $tableItem;
            }
            else
                $this->Tables[$tName] = $tableItem;
        }
    }

    /**
     * @param string $tableName
     * @return string
     */
    public function CreateTableSyntax($tableName='')
    {
        if ($tableName != '')
        {
            $tSpec = null;
            foreach ($this->Tables as $item)
            {
                if (strtolower($item['name']) == strtolower($tableName))
                    return $this->_CreateTableSyntax($item);
            }
            return '';
        }
        else
        {
            $s = '';
            foreach ($this->Tables as $item)
            {
                $s .= $this->_CreateTableSyntax($item);
                $s .= "\n\n";
            }
            return $s;
        }
    }

    /**
     * @param string $tableName
     * @param bool $updateLog
     * @param bool $changeLog
     * @return string
     */
    public function CreateTableTriggerSyntax($tableName='',$updateLog=true,$changeLog=false)
    {
        if ($tableName != '')
        {
            $tSpec = null;
            foreach ($this->Tables as $item)
            {
                if (strtolower($item['name']) == strtolower($tableName))
                {
                    $s  = $this->_CreateAfterInsertTrigger($item,$updateLog,$changeLog);
                    $s .= "\n";
                    $s .= $this->_CreateAfterUpdateTrigger($item,$updateLog,$changeLog);
                    $s .= "\n";
                    $s .= $this->_CreateAfterDeleteTrigger($item,$updateLog,$changeLog);
                    return $s;
                }
            }
            return '';
        }
        else
        {
            $s = '';
            foreach ($this->Tables as $item)
            {
                $s .= $this->_CreateAfterInsertTrigger($item,$updateLog,$changeLog);
                $s .= "\n";
                $s .= $this->_CreateAfterUpdateTrigger($item,$updateLog,$changeLog);
                $s .= "\n";
                $s .= $this->_CreateAfterDeleteTrigger($item,$updateLog,$changeLog);
                $s .= "\n\n";
            }
            return $s;
        }
    }
}
?>