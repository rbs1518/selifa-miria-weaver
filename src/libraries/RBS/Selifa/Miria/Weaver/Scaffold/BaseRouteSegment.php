<?php
namespace RBS\Selifa\Miria\Weaver\Scaffold;

/**
 * Class BaseRouteSegment
 * @package RBS\Selifa\Miria\Weaver\Scaffold
 */
abstract class BaseRouteSegment
{
    /**
     * @var null|RouteSegment
     */
    public $P = null;

    /**
     * @var null|RouteSegment
     */
    public $N = null;

    /**
     * @var null|RouteSegment
     */
    public $F = null;

    /**
     * @var null|RouteSegment
     */
    public $L = null;
}
?>