<?php
namespace RBS\Selifa\Miria\Weaver\Scaffold;
use RBS\Selifa\Miria\Weaver\Generator;

/**
 * Class Route
 * @package RBS\Selifa\Miria\Weaver\Scaffold
 */
class Route extends BaseRouteSegment
{
    /**
     * @var array
     */
    protected $POs = array(
        ':digit' => 1,
        ':char' => 2,
        ':hex' => 3,
        ':alpha' => 4,
        ':std' => 5,
        ':default:' => 98,
        '*' => 99
    );

    /**
     * @var array
     */
    protected $POKeys = array();

    /**
     * @param array $routeData
     */
    public function __construct($routeData)
    {
        $this->TranslateFromHierarchArray($routeData,$this);
        $this->POKeys = array_keys($this->POs);
    }

    /**
     * @param BaseRouteSegment $segment
     * @param BaseRouteSegment $ts
     * @param BaseRouteSegment $newSegment
     */
    private function _SCheckPrevious($segment,$ts,$newSegment)
    {
        if ($ts->P == null)
        {
            $segment->F = $newSegment;
            $newSegment->N = $ts;
            $ts->P = $newSegment;
        }
        else
        {
            $newSegment->N = $ts;
            $newSegment->P = $ts->P;
            $ts->P->N = $newSegment;
            $ts->P = $newSegment;
        }
    }

    /**
     * @param array $keyArray
     * @param int $aIndex
     * @param BaseRouteSegment $segment
     * @param string $target
     * @param string $method
     */
    protected function PutKey($keyArray,$aIndex,BaseRouteSegment $segment,$target,$method='')
    {
        if (!isset($keyArray[$aIndex]))
            return;

        $theKey = trim($keyArray[$aIndex]);
        if ($theKey == '*')
        {
            $newSegment = new RouteSegment($theKey);
            $newSegment->AddTarget($target,$method);

            if ($segment->F == null)
            {
                $segment->F = $newSegment;
                $segment->L = $newSegment;
            }
            else
            {
                $lastSegment = $segment->L;
                $segment->L = $newSegment;
                $lastSegment->N = $newSegment;
                $newSegment->P = $lastSegment;
            }
        }
        else
        {
            $newSegment = new RouteSegment($theKey);
            if ($segment->F == null)
            {
                $segment->F = $newSegment;
                $segment->L = $newSegment;
            }
            else
            {
                $isParam = in_array(substr($theKey,0,1),array(':','*'));
                if ($isParam)
                {
                    $newSegment->Parameter = $theKey;
                    $ts = $segment->F;
                    while (true)
                    {
                        if (in_array(substr($ts->Key,0,1),array(':','*')))
                        {
                            $tso = 50;
                            if (isset($this->POs[$ts->Key]))
                                $tso = $this->POs[$ts->Key];

                            $nso = 50;
                            if (isset($this->POs[$newSegment->Key]))
                                $nso = $this->POs[$newSegment->Key];

                            //echo ('(TSO['.substr($ts->Key,0,1).']: '.$tso.', NSO['.substr($theKey,0,1).']: '.$nso.")\n");
                            if ($nso < $tso)
                            {
                                //echo "(NSO < TSO)\n";
                                $this->_SCheckPrevious($segment,$ts,$newSegment);
                                break;
                            }
                            else
                            {
                                if ($ts->N == null)
                                {
                                    $ts->N = $newSegment;
                                    $newSegment->P = $ts;
                                    $segment->L = $newSegment;
                                    break;
                                }
                                else
                                    $ts = $ts->N;
                            }
                        }
                        else
                        {
                            if ($ts->N == null)
                            {
                                $ts->N = $newSegment;
                                $newSegment->P = $ts;
                                $segment->L = $newSegment;
                                break;
                            }
                            else
                                $ts = $ts->N;
                        }
                    }
                }
                else
                {
                    $ts = $segment->F;
                    while (true)
                    {
                        if (in_array(substr($ts->Key,0,1),array(':','*')))
                        {
                            $this->_SCheckPrevious($segment,$ts,$newSegment);
                            break;
                        }
                        else
                        {
                            if (strcmp($newSegment->Key,$ts->Key) > 0)
                            {
                                //$newSegment->Key > $ts->Key
                                //echo ("(".$newSegment->Key." > ".$ts->Key.")\n");
                                if ($ts->N == null)
                                {
                                    $ts->N = $newSegment;
                                    $newSegment->P = $ts;
                                    $segment->L = $newSegment;
                                    break;
                                }
                                else
                                {
                                    $ts = $ts->N;
                                }
                            }
                            else
                            {
                                //$newSegment->Key < $ts->Key
                                //echo ("(".$newSegment->Key." < ".$ts->Key.")\n");
                                $this->_SCheckPrevious($segment,$ts,$newSegment);
                                break;
                            }
                        }
                    }
                }
            }

            if (!isset($keyArray[$aIndex+1]))
                $newSegment->AddTarget($target,$method);
            else
                $this->PutKey($keyArray,($aIndex+1),$newSegment,$target,$method);
        }
    }

    /**
     * @param array $keyArray
     * @param int $aIndex
     * @param BaseRouteSegment $segment
     * @param string $target
     * @param string $method
     */
    protected function TraverseSegment($keyArray,$aIndex,BaseRouteSegment $segment,$target,$method='')
    {
        if (!isset($keyArray[$aIndex]))
        {
            $oSegment = new RouteSegment(':default:');
            $oSegment->AddTarget($target,$method);

            if ($segment->F != null)
            {
                $oSegment->N = $segment->F;
                $segment->F->P = $oSegment;
                $segment->F = $oSegment;
            }
            else
            {
                $segment->F = $oSegment;
                $segment->L = $oSegment;
            }
            return;
        }

        $theKey = trim($keyArray[$aIndex]);
        if ($segment->F != null)
        {
            $current = $segment->F;
            while ($current != null)
            {
                //if ($current instanceof RouteSegment)
                //    echo "TEST: ".$theKey." - ".$current->Key."\n";
                if ($current->Key == $theKey)
                {
                    if ($current->F != null)
                    {
                        $this->TraverseSegment($keyArray,($aIndex+1),$current,$target,$method);
                    }
                    else
                    {
                        if (isset($keyArray[$aIndex+1]))
                        {
                            if ($current->Targets != null)
                            {
                                $pTarget = $current->Targets;
                                $this->PutKey($keyArray,$aIndex+1,$current,$target,$method);
                                $oSegment = new RouteSegment(':default:');
                                $oSegment->SetTarget($pTarget);

                                if ($current->F != null)
                                {
                                    $oSegment->N = $current->F;
                                    $current->F->P = $oSegment;
                                    $current->F = $oSegment;
                                }
                                else
                                {
                                    $current->F = $oSegment;
                                    $current->L = $oSegment;
                                }
                            }
                            else
                                $this->PutKey($keyArray,$aIndex+1,$current,$target,$method);
                        }
                        else
                            $current->AddTarget($target,$method);
                    }
                    break;
                }
                $current = $current->N;
            }

            if ($current == null)
                $this->PutKey($keyArray,$aIndex,$segment,$target,$method);

            /*if (isset($keyArray[$aIndex+1]))
            {
                $current = $segment->F;
                while ($current != null)
                {
                    if ($current->Key == $theKey)
                    {
                        if ($current->F != null)
                            $this->TraverseSegment($keyArray,($aIndex+1),$current,$target,$method);
                        else
                            $current->AddTarget($target,$method);
                        break;
                    }$current = $current->N;
                }

                if ($current == null)
                    $this->PutKey($keyArray,$aIndex,$segment,$target,$method);
            }
            else
            {
                $oSegment = new RouteSegment(':default:');
                $oSegment->AddTarget($target,$method);

                $oSegment->N = $segment->F;
                $segment->F->P = $oSegment;
                $segment->F = $oSegment;
            }*/
        }
        else
        {
            if (isset($keyArray[$aIndex+1]))
            {
                if ($segment instanceof RouteSegment)
                {
                    if ($segment->Targets !== null)
                    {
                        $oSegment = new RouteSegment(':default:');
                        if (is_array($segment->Targets))
                        {
                            foreach ($segment->Targets as $m => $t)
                                $oSegment->AddTarget($t,$m);
                        }
                        else
                            $oSegment->AddTarget($segment->Targets);

                        $segment->Targets = null;
                        $segment->F = $oSegment;
                        $segment->L = $oSegment;
                    }
                }
                $this->PutKey($keyArray,$aIndex,$segment,$target,$method);
            }
            else
            {
                if ($segment instanceof RouteSegment)
                    $segment->AddTarget($target,$method);
                else
                    $this->PutKey($keyArray,$aIndex,$segment,$target,$method);
            }
        }
    }

    /*protected function TraverseAndGetSegment($keyArray,$aIndex,BaseRouteSegment $segment)
    {
        if (!isset($keyArray[$aIndex]))
            return $segment;

        $theKey = trim($keyArray[$aIndex]);
        if ($segment->F != null)
        {
            $current = $segment->F;
            while ($current != null)
            {
                if ($current instanceof RouteSegment)
                    echo "MATCH ".$theKey." AGAINST ".$current->Key."\n";
                if ($current->Key == $theKey)
                {





                    if ($current->F != null)
                    {
                        $this->TraverseSegment($keyArray,($aIndex+1),$current,$target,$method);
                    }
                    else
                    {
                        if (isset($keyArray[$aIndex+1]))
                        {
                            if ($current->Targets != null)
                            {
                                $pTarget = $current->Targets;
                                $this->PutKey($keyArray,$aIndex+1,$current,$target,$method);
                                $oSegment = new RouteSegment(':default:');
                                $oSegment->SetTarget($pTarget);

                                if ($current->F != null)
                                {
                                    $oSegment->N = $current->F;
                                    $current->F->P = $oSegment;
                                    $current->F = $oSegment;
                                }
                                else
                                {
                                    $current->F = $oSegment;
                                    $current->L = $oSegment;
                                }
                            }
                            else
                                $this->PutKey($keyArray,$aIndex+1,$current,$target,$method);
                        }
                        else
                            $current->AddTarget($target,$method);
                    }
                    break;
                }
                $current = $current->N;
            }

            if ($current == null)
                $this->PutKey($keyArray,$aIndex,$segment,$target,$method);
        }
        else
        {
            if (isset($keyArray[$aIndex+1]))
            {
                if ($segment instanceof RouteSegment)
                {
                    if ($segment->Targets !== null)
                    {
                        $oSegment = new RouteSegment(':default:');
                        if (is_array($segment->Targets))
                        {
                            foreach ($segment->Targets as $m => $t)
                                $oSegment->AddTarget($t,$m);
                        }
                        else
                            $oSegment->AddTarget($segment->Targets);

                        $segment->Targets = null;
                        $segment->F = $oSegment;
                        $segment->L = $oSegment;
                    }
                }
                $this->PutKey($keyArray,$aIndex,$segment,$target,$method);
            }
            else
            {
                //var_dump('test_'.$aIndex);
                if ($segment instanceof RouteSegment)
                    $segment->AddTarget($target,$method);
            }
        }



    }*/

    /**
     * @param RouteSegment $segment
     * @param array $array
     * @param string $line
     * @param int $level
     */
    protected function TranslateIntoLineArray(RouteSegment $segment,&$array,$line,$level)
    {
        $current = $segment;
        while ($current != null)
        {
            if ($current->F != null)
            {
                $nextLine = ($line.$current->Key.'/');
                $this->TranslateIntoLineArray($current->F,$array,$nextLine,($level+1));
            }
            else
            {
                if ($current->Key == ':default:')
                {
                    $eol = substr($line,-1);
                    if ($eol == '/')
                        $cLine = substr($line,0,-1);
                    else
                        $cLine = $line;
                    $array[$cLine] = $current->Targets;
                }
                else
                {
                    $nextLine = ($line.$current->Key);
                    $array[$nextLine] = $current->Targets;
                }
            }

            $current = $current->N;
        }
    }

    /**
     * @param array $array
     * @param BaseRouteSegment $segment
     */
    protected function TranslateFromHierarchArray($array,BaseRouteSegment $segment)
    {
        $lastSegment = null;
        foreach ($array as $key => $item)
        {
            $newSegment = new RouteSegment($item['key'],$item['parameter']);
            if (isset($item['target']))
            {
                if (is_array($item['target']))
                {
                    foreach ($item['target'] as $method => $target)
                        $newSegment->AddTarget($target,$method);
                }
                else
                    $newSegment->AddTarget($item['target']);
            }

            if ($lastSegment == null)
                $segment->F = $newSegment;
            else
            {
                $newSegment->P = $lastSegment;
                $lastSegment->N = $newSegment;
            }

            $lastSegment = $newSegment;
            if (isset($item['childs']))
                $this->TranslateFromHierarchArray($item['childs'],$newSegment);
        }

        if ($lastSegment != null)
            $segment->L = $lastSegment;
    }

    /**
     * @param RouteSegment $segment
     * @param array $array
     */
    protected function TranslateIntoHierarchArray(RouteSegment $segment,&$array)
    {
        $current = $segment;
        while ($current != null)
        {
            if ($current->F != null)
            {
                $childArray = array();
                $this->TranslateIntoHierarchArray($current->F,$childArray);
                $array[$current->Key] = array(
                    'key' => $current->Key,
                    'parameter' => $current->Parameter,
                    'childs' => $childArray
                );
            }
            else
            {
                $array[$current->Key] = array(
                    'key' => $current->Key,
                    'parameter' => $current->Parameter,
                    'target' => $current->Targets
                );
            }

            $current = $current->N;
        }
    }

    /**
     * @param string $pattern
     * @param string $target
     * @param string $method
     */
    public function Add($pattern,$target,$method='')
    {
        $keys = explode('/',$pattern);
        $this->TraverseSegment($keys,0,$this,$target,$method);
    }

    /**
     * @return array
     */
    public function GetRouteDataAsArray()
    {
        $routes = array();
        if ($this->F != null)
            $this->TranslateIntoHierarchArray($this->F,$routes);
        return $routes;
    }

    /**
     * @return array
     */
    public function GetList()
    {
        $routes = array();
        if ($this->F != null)
            $this->TranslateIntoLineArray($this->F,$routes,'',0);
        return $routes;
    }

    /**
     * @param string $baseKey
     */
    public function WriteList($baseKey='')
    {
        $routes = array();
        if ($this->F != null)
            $this->TranslateIntoLineArray($this->F,$routes,'',0);

        if (count($routes) <= 0)
            echo "No route defined.\n";
        else
        {
            foreach ($routes as $route => $target)
            {
                echo (" - ".$route);
                if (is_array($target))
                {
                    echo "\n";
                    foreach ($target as $method => $realTarget)
                        echo "\t".strtoupper($method)." => ".$realTarget."\n";
                }
                else
                {
                    if ($target == '')
                        echo "\n\n";
                    else
                        echo (" => ".$target."\n");
                }

            }
        }
    }

    /**
     * @param string $appPath
     */
    public function GenerateFile($appPath)
    {
        $gen = new Generator();
        $routeFile = ($appPath.'/routes.php');

        $routes = array();
        if ($this->F != null)
            $this->TranslateIntoLineArray($this->F,$routes,'',0);

        $content = $gen->CreateRouteFileContent($routes);
        file_put_contents($routeFile,$content);
    }
}
?>