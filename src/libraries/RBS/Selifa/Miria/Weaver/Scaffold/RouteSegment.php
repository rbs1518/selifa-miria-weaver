<?php
namespace RBS\Selifa\Miria\Weaver\Scaffold;

/**
 * Class RouteSegment
 * @package RBS\Selifa\Miria\Weaver
 */
class RouteSegment extends BaseRouteSegment
{
    /**
     * @var string
     */
    public $Key = '';

    /**
     * @var string
     */
    public $Parameter = '';

    /**
     * @var array|string|null
     */
    public $Targets = null;

    /**
     * @param string $key
     * @param string $parameter
     */
    public function __construct($key,$parameter='')
    {
        $this->Key = $key;
        $this->Parameter = $parameter;
    }

    /**
     * @param array|string|null $targets
     */
    public function SetTarget($targets)
    {
        $this->Targets = $targets;
    }

    /**
     * @param $target
     * @param string $method
     */
    public function AddTarget($target,$method='')
    {
        if ($method == '')
        {
            if (is_array($this->Targets))
                $this->Targets['GET'] = $target;
            else
                $this->Targets = $target;
        }
        else
        {
            if ($this->Targets === null)
            {
                $this->Targets = array(
                    $method => $target
                );
            }
            else
            {
                if (is_array($this->Targets))
                    $this->Targets[$method] = $target;
                else
                {
                    $this->Targets = array(
                        'GET' => (string)$this->Targets,
                        $method => $target
                    );
                }
            }
        }
    }
}
?>