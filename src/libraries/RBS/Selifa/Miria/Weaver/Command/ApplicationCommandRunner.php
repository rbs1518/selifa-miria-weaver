<?php
namespace RBS\Selifa\Miria\Weaver\Command;
use RBS\Selifa\Miria\Weaver\ApplicationGenerator;
use RBS\Selifa\Miria\Weaver\CommandRunner;
use RBS\Selifa\Miria\Weaver\IO\Console;
use RBS\Selifa\Miria\Weaver\IO\ConsoleFormatParser;
use RBS\Selifa\Miria\Weaver\SelifaApplicationScaffolding;
use Exception;

/**
 * Class ApplicationCommandRunner
 * @package RBS\Selifa\Miria\Weaver\Command
 */
class ApplicationCommandRunner extends CommandRunner
{
    /**
     * @var null|SelifaApplicationScaffolding
     */
    protected $SAS = null;

    /**
     * @var bool
     */
    protected $QuietMode = false;

    /**
     *
     */
    protected function Initialize()
    {
        // TODO: Implement Initialize() method.
    }

    /**
     * @param string $appKey
     * @param bool $disableSave
     * @param bool $quietMode
     * @throws Exception
     */
    public function SetApplication($appKey,$disableSave=false,$quietMode=false)
    {
        $appPath = $this->Weaver->GetSelifaApplicationPath($appKey);
        if (!file_exists($appPath))
            throw new Exception('Application does not exists. Please initialize the app first.');

        $this->SAS = new SelifaApplicationScaffolding($this->Weaver,$appKey,$disableSave);
        $this->QuietMode = $quietMode;
    }

    public function update()
    {
        $this->SAS->UpdateApplication();
    }

    public function remove()
    {
        $b = Console::Confirm("Are you sure want to remove '{!f(light_green)}".$this->SAS->GetKey()."{/f}' application?");
        if (!$b)
        {
            Console::WriteLine('Application removal cancelled.');
            return;
        }

        $doBackup = Console::Confirm('Are you want to backup this application?','y');
        $this->SAS->RemoveApplication($doBackup);
    }

    public function backup()
    {
        $this->SAS->BackupApplication();
    }
}
?>