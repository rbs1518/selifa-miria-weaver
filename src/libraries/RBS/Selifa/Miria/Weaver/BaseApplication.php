<?php
namespace RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver;

/**
 * Class BaseApplication
 * @package RBS\Selifa\Miria\Weaver
 */
abstract class BaseApplication
{
    /**
     * @var bool
     */
    public $IsUserApplication = false;

    /**
     * @param Weaver $weaver
     * @param array $options
     */
    abstract public function Initialize(Weaver $weaver,$options);

    /**
     * @param Weaver $weaver
     * @param string $command
     * @param array $parameters
     */
    abstract public function Run(Weaver $weaver, $command,$parameters);

    /**
     * @return string
     */
    abstract public function GetShortDescription();

    /**
     * @param Weaver $weaver
     */
    public function Finalize(Weaver $weaver) { }
}
?>