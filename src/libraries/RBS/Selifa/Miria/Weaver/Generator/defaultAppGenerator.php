<?php
namespace RBS\Selifa\Miria\Weaver\Generator;
use RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver\ApplicationGenerator;
use RBS\Selifa\Miria\Weaver\SelifaApplicationScaffolding;
use RBS\Selifa\Miria\Weaver\IO\Console;
use RBS\Selifa\Miria\Weaver\IO\File;
use RBS\Utility\F;
use RBS\Utility\Inflector;

/**
 * Class defaultAppGenerator
 * @package RBS\Selifa\Miria\Weaver\Generator
 */
class defaultAppGenerator extends ApplicationGenerator
{
    /**
     * @return string
     */
    public function GetType()
    {
        return 'default';
    }

    /**
     * @param Weaver $weaver
     * @param SelifaApplicationScaffolding $sas
     * @param array $aOptions
     * @param array $aParams
     * @param bool $isQuietMode
     */
    public function GetCustomParameter(Weaver $weaver, SelifaApplicationScaffolding $sas, $aOptions, &$aParams, $isQuietMode = false)
    {
        if ($isQuietMode)
        {
            if (isset($aOptions['lang']))
                $aParams['DefaultLang'] = strtolower(trim($aOptions['lang']));
            else
                $aParams['DefaultLang'] = 'en';
            if (isset($aOptions['create-bc']))
            {
                $aParams['IsCBC'] = true;
                if (isset($aOptions['bc-name']))
                    $aParams['BCName'] = trim($aOptions['bc-name']);
                else
                    $aParams['BCName'] = ('Base'.Inflector::camelize($aParams['Key']).'Controller');
            }
            else
                $aParams['IsCBC'] = false;
        }
        else
        {
            $aParams['DefaultLang'] = Console::AskForValueWithDefault('Default template language?','en');
            $aParams['IsCBC'] = Console::Confirm('Create base controller class?');
            if ($aParams['IsCBC'])
            {
                $bcName = ('Base'.Inflector::camelize($aParams['Key']).'Controller');
                $aParams['BCName'] = Console::AskForValueWithDefault('Base controller class name?',$bcName);
            }
        }
    }

    /**
     * @param Weaver $weaver
     * @param SelifaApplicationScaffolding $sas
     * @param array $aParams
     * @param array $aContent
     * @param array $appData
     */
    public function Create(Weaver $weaver, SelifaApplicationScaffolding $sas, $aParams, &$aContent, &$appData)
    {
        $appPath = ($weaver->GetSelifaApplicationPath($aParams['Key'],false));
        $defPaths = F::Extract($weaver->ComposeData,'default-paths',array());

        foreach ($defPaths as $pathKey => $path)
        {
            $_path = ($appPath.DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR);
            if (!file_exists($_path))
                mkdir($_path,0775,true);
            $appData['paths'][$pathKey] = $_path;
        }

        $dns = ($aParams['RootNS']."\\Controller\\".Inflector::camelize($aParams['Key']));
        $appData['config']['rbs_selifa_miria_miria']['DefaultNamespace'] = $dns;

        $rAppPath = $weaver->GetSelifaApplicationPath($aParams['Key'],false,false);

        $ctrlPath = ($rAppPath.DIRECTORY_SEPARATOR.'controllers');
        if (isset($defPaths['controller']))
            $ctrlPath = ($rAppPath.DIRECTORY_SEPARATOR.$defPaths['controller']);
        $appData['config']['rbs_selifa_miria_miria']['ControllerPath'] = ($ctrlPath.DIRECTORY_SEPARATOR);

        $tptPath = ($rAppPath.DIRECTORY_SEPARATOR.'templates');
        if (isset($defPaths['template']))
            $tptPath = ($rAppPath.DIRECTORY_SEPARATOR.$defPaths['template']);
        $appData['config']['rbs_selifa_miria_miria']['ControllerOptions']['default']['TemplatePath'] = ($tptPath.DIRECTORY_SEPARATOR);

        $dfLang = F::Extract($aParams,'DefaultLang','en');
        $appData['config']['rbs_selifa_miria_miria']['ControllerOptions']['default']['DefaultLanguage'] = $dfLang;

        $tplPaths = array(
            ('contents'.DIRECTORY_SEPARATOR.'main'),
            'parts',
            'layouts'
        );

        foreach ($tplPaths as $tplp)
        {
            $ftplPath = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR.$tptPath.DIRECTORY_SEPARATOR.$dfLang.DIRECTORY_SEPARATOR.$tplp);
            if (!file_exists($ftplPath))
                mkdir($ftplPath,0775,true);
        }

        if ($aParams['IsCBC'])
        {
            $bc_fc = $this->ParseTemplate('bc',array(
                'ns' => ($aParams['RootNS']."\\Controller"),
                'cn' => $aParams['BCName']
            ));

            $rnsDir = ($weaver->GetSelifaLibraryPath().DIRECTORY_SEPARATOR.$aParams['RootNS'].DIRECTORY_SEPARATOR.'Controller');
            $bc_ffp = ($rnsDir.DIRECTORY_SEPARATOR.$aParams['BCName'].'.php');
            File::Put($bc_ffp,$bc_fc);

            $parentFNS = ($aParams['RootNS'].'\\Controller\\'.$aParams['BCName']);
            $parentBCN = $aParams['BCName'];
        }
        else
        {
            $parentFNS = ('RBS\Selifa\Miria\DefaultMiriaController');
            $parentBCN = 'DefaultMiriaController';
        }

        $fnCtrlPath = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR.$appData['config']['rbs_selifa_miria_miria']['ControllerPath']);
        $m_fc = $this->ParseTemplate('main',array(
            'ns' => $dns,
            'bns' => $parentFNS,
            'bcn' => $parentBCN
        ));

        $m_ffp = ($fnCtrlPath.'main.php');
        File::Put($m_ffp,$m_fc);

        $m_htpl_path = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR.$tptPath.DIRECTORY_SEPARATOR.$dfLang.DIRECTORY_SEPARATOR.'contents'.DIRECTORY_SEPARATOR.'main'.DIRECTORY_SEPARATOR.'main.html');
        $m_htpl = $this->ParseTemplate('main_html',array(
            'appname' => F::Extract($aParams,'Name','')
        ));
        File::Put($m_htpl_path,$m_htpl,false);

        $str_htpl_path = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR.$tptPath.DIRECTORY_SEPARATOR.$dfLang.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.'master.html');
        $str_htpl = $this->ParseTemplate('master_html',array(
            'appname' => F::Extract($aParams,'Name',''),
            'author' => ''
        ));
        File::Put($str_htpl_path,$str_htpl,false);

        $idx_fc = $this->ParseTemplate('aidx');
        $aContent['index.php'] .= $idx_fc;
    }

    public function Update(Weaver $weaver, SelifaApplicationScaffolding $sas, $action)
    {
        // TODO: Implement Update() method.
    }

    public function Remove(Weaver $weaver, SelifaApplicationScaffolding $sas)
    {
        // TODO: Implement Remove() method.
    }
}
?>