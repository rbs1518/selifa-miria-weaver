<?php
namespace RBS\Selifa\Miria\Weaver\Generator;
use RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver\ApplicationGenerator;
use RBS\Selifa\Miria\Weaver\SelifaApplicationScaffolding;
use RBS\Selifa\Miria\Weaver\IO\Console;
use RBS\Utility\Inflector;
use RBS\Utility\F, Exception;

/**
 * Class rpcAppGenerator
 * @package RBS\Selifa\Miria\Weaver\Generator
 */
class rpcAppGenerator extends ApplicationGenerator
{
    public function GetType()
    {
        return 'rpc';
    }

    /**
     * @param Weaver $weaver
     * @param SelifaApplicationScaffolding $sas
     * @param array $aOptions
     * @param array $aParams
     * @param bool $isQuietMode
     */
    public function GetCustomParameter(Weaver $weaver, SelifaApplicationScaffolding $sas, $aOptions, &$aParams, $isQuietMode = false)
    {
        if ($isQuietMode)
        {
            if (isset($aOptions['lang']))
                $aParams['DefaultLang'] = strtolower(trim($aOptions['lang']));
            else
                $aParams['DefaultLang'] = 'en';
            if (isset($aOptions['create-bc']))
            {
                $aParams['IsCBC'] = true;
                if (isset($aOptions['bc-name']))
                    $aParams['BCName'] = trim($aOptions['bc-name']);
                else
                    $aParams['BCName'] = ('Base'.Inflector::camelize($aParams['Key']).'Controller');
            }
            else
                $aParams['IsCBC'] = false;
        }
        else
        {
            $aParams['DefaultLang'] = Console::AskForValueWithDefault('Default template language?','en');
            $aParams['IsCBC'] = Console::Confirm('Create base controller class?');
            if ($aParams['IsCBC'])
            {
                $bcName = ('Base'.Inflector::camelize($aParams['Key']).'Controller');
                $aParams['BCName'] = Console::AskForValueWithDefault('Base controller class name?',$bcName);
            }
        }
    }

    public function Create(Weaver $weaver, SelifaApplicationScaffolding $sas, $aParams, &$aContent, &$appData)
    {
        $appPath = ($weaver->GetSelifaApplicationPath($aParams['Key'],false));
        $defPaths = F::Extract($weaver->ComposeData,'default-paths',array());

        foreach ($defPaths as $pathKey => $path)
        {
            $_path = ($appPath.DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR);
            if (!file_exists($_path))
                mkdir($_path,0775,true);
            $appData['paths'][$pathKey] = $_path;
        }

        $dns = ($aParams['RootNS']."\\Controller\\".Inflector::camelize($aParams['Key']));
        $appData['config']['rbs_selifa_miria_miria']['DefaultNamespace'] = $dns;

        $ctrlPath = ('controllers'.DIRECTORY_SEPARATOR);
        if (isset($defPaths['controller']))
            $ctrlPath = ($weaver->GetSelifaApplicationPath($aParams['Key'],false,false).DIRECTORY_SEPARATOR.$ctrlPath);
        $appData['config']['rbs_selifa_miria_miria']['ControllerPath'] = $ctrlPath;

        if ($aParams['IsCBC'])
        {
            $rnsDir = ($weaver->GetSelifaLibraryPath().DIRECTORY_SEPARATOR.$aParams['RootNS'].DIRECTORY_SEPARATOR.'Controller');
            if (!file_exists($rnsDir))
                mkdir($rnsDir,0777,true);

            $bc_fc = $this->ParseTemplate('bc',array(
                'ns' => ($aParams['RootNS']."\\Controller"),
                'cn' => $aParams['BCName']
            ));

            $bc_ffp = ($rnsDir.DIRECTORY_SEPARATOR.$aParams['BCName'].'.php');
            if (!file_exists($bc_ffp))
                file_put_contents($bc_ffp,$bc_fc);

            $parentFNS = ($aParams['RootNS'].'\\Controller\\'.$aParams['BCName']);
            $parentBCN = $aParams['BCName'];
        }
        else
        {
            $parentFNS = ('RBS\Selifa\Miria\BaseRPCController');
            $parentBCN = 'BaseRPCController';
        }

        $fnCtrlPath = ($appPath.DIRECTORY_SEPARATOR.$ctrlPath);
        $m_fc = $this->ParseTemplate('main',array(
            'ns' => $dns,
            'bns' => $parentFNS,
            'bcn' => $parentBCN
        ));
        $m_ffp = ($fnCtrlPath.'main.php');
        if (!file_exists($m_ffp))
            file_put_contents($m_ffp,$m_fc);

        $idx_fc = $this->ParseTemplate('aidx');
        $aContent['index.php'] .= $idx_fc;
    }

    public function Update(Weaver $weaver, SelifaApplicationScaffolding $sas, $action)
    {
        // TODO: Implement Update() method.
    }

    public function Remove(Weaver $weaver, SelifaApplicationScaffolding $sas)
    {
        // TODO: Implement Remove() method.
    }
}
?>