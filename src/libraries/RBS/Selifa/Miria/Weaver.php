<?php
namespace RBS\Selifa\Miria;
use Exception, CommandLineArgument;
use RBS\Selifa\Miria\Weaver\BaseApplication;
use RBS\Selifa\Miria\Weaver\IO\Console;
use RBS\Selifa\Miria\Weaver\IO\ConsoleFormatParser;
use RBS\Selifa\Miria\Weaver\IO\Directory;

/**
 * Class Weaver
 * @package RBS\Selifa\Miria
 */
class Weaver
{
    /**
     * @var null|Weaver
     */
    private static $_Instance = null;

    /**
     * @var array
     */
    protected $_Options = array(
        'BasePath' => 'selifa/miria-weaver/',
        'UserPath' => 'weaver/',
        'ScaffoldingPath' => 'scaffolds/',

        //'ApplicationPath' => 'selifa/miria-weaver/apps',
        'ApplicationPath' => 'apps',
        //'TemplatePath' => 'selifa/miria-weaver/templates',
        'TemplatePath' => 'templates',
        //'ReflectorPath' => 'selifa/miria-weaver/reflectors',
        'ReflectorPath' => 'reflectors',

        'ConfigPath' => 'configs',
        'DefaultNamespace' => 'WeaverApplication',
        'ReflectorDefaultNamespace' => 'WeaverSQLReflector',
        'ClassSuffix' => 'Application',
        'ReflectorClassSuffix' => 'Reflector'
    );

    /**
     * @var string
     */
    protected $_SelifaComposeFile = '';

    /**
     * @var null|array
     */
    public $ComposeData = null;

    /**
     * @var null|array
     */
    public $InstallData = null;

    /**
     * @param array $options
     * @return null|Weaver
     */
    public static function Initialize($options=array())
    {
        if (self::$_Instance == null)
            self::$_Instance = new Weaver($options);
        return self::$_Instance;
    }

    /**
     * Weaver constructor.
     * @param array $options
     */
    private function __construct($options)
    {
        $this->_Options = array_replace_recursive($this->_Options,$options);
        ConsoleFormatParser::Initialize();
    }

    /**
     *
     */
    function __destruct()
    {
        $this->WriteComposeData();
    }

    /**
     * @param array $cData
     * @param string $scFile
     */
    public function SetComposeData(&$cData,$scFile)
    {
        $this->ComposeData = $cData;
        $this->_SelifaComposeFile = $scFile;

        $scfPath = $cData['weaver']['scaffolding-path'];
        $this->_Options['ScaffoldingPath'] = ($scfPath.DIRECTORY_SEPARATOR);

        if (isset($cData['weaver']['user-path']))
        {
            $usrPath = $cData['weaver']['user-path'];
            $this->_Options['UserPath'] = ($usrPath.DIRECTORY_SEPARATOR);
        }

        if (isset($cData['weaver']['config-path']))
        {
            $cfgPath = $cData['weaver']['config-path'];
            $this->_Options['ConfigPath'] = ($cfgPath.DIRECTORY_SEPARATOR);
        }
    }

    /**
     * @param string $key
     * @param mixed|null $default
     * @return mixed|null
     */
    public function GetOption($key,$default=null)
    {
        if (isset($this->_Options[$key]))
            return $this->_Options[$key];
        else
            return $default;
    }

    /**
     * @param string $key
     * @param mixed|null $default
     * @return mixed|null
     */
    public function GetGlobal($key,$default=null)
    {
        if (isset($this->_Globals[$key]))
            return $this->_Globals[$key];
        else
            return $default;
    }

    /**
     * @param string $jsonId
     * @return bool
     */
    public function IsScaffoldingJSONExists($jsonId)
    {
        $jsonId = strtolower(trim($jsonId));
        $file = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR.$this->_Options['ScaffoldingPath'].$jsonId.'.json');
        return file_exists($file);
    }

    /**
     * @param string $jsonId
     * @return array|mixed
     */
    public function ReadScaffoldingJSON($jsonId)
    {
        $jsonId = strtolower(trim($jsonId));
        $file = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR.$this->_Options['ScaffoldingPath'].$jsonId.'.json');
        if (!file_exists($file))
            return array();
        else
        {
            $content = file_get_contents($file);
            return json_decode($content,true);
        }
    }

    /**
     * @param string $jsonId
     * @param array $data
     */
    public function WriteScaffoldingJSON($jsonId,$data)
    {
        $jsonId = strtolower(trim($jsonId));
        $file = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR.$this->_Options['ScaffoldingPath'].$jsonId.'.json');
        $content = json_encode($data,JSON_PRETTY_PRINT);
        file_put_contents($file,$content);
    }

    #region Path Utilities

    /**
     * @param bool $isUser
     * @param bool $useSeparator
     * @return string
     */
    public function GetApplicationPath($isUser=false,$useSeparator=true)
    {
        $s = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR);
        if ($isUser)
            $s .= $this->_Options['UserPath'];
        else
            $s .= $this->_Options['BasePath'];
        return ($s.$this->_Options['ApplicationPath'].($useSeparator?DIRECTORY_SEPARATOR:''));
    }

    /**
     * @param bool $isUser
     * @param bool $useSeparator
     * @return string
     */
    public function GetTemplatePath($isUser=false,$useSeparator=true)
    {
        $s = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR);
        if ($isUser)
            $s .= $this->_Options['UserPath'];
        else
            $s .= $this->_Options['BasePath'];
        return ($s.$this->_Options['TemplatePath'].($useSeparator?DIRECTORY_SEPARATOR:''));
    }

    /**
     * @param bool $isUser
     * @param bool $useSeparator
     * @return string
     */
    public function GetReflectorPath($isUser=false,$useSeparator=true)
    {
        $s = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR);
        if ($isUser)
            $s .= $this->_Options['UserPath'];
        else
            $s .= $this->_Options['BasePath'];
        return ($s.$this->_Options['ReflectorPath'].($useSeparator?DIRECTORY_SEPARATOR:''));
    }

    /**
     * @return string
     */
    public function GetConfigPath()
    {
        $s = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR);
        return ($s.$this->_Options['ScaffoldingPath'].$this->_Options['ConfigPath']);
    }
    #endregion

    /**
     *
     */
    public function OutputAbout()
    {
        Console::WriteLine('{!f(light_green)}'.SELIFA_NAME.'{/f} '.SELIFA);
        Console::WriteLine('(c){!f(light_cyan)}2017{/f}. Rinardi B. Sarean.');
        Console::WriteLine('This application is under MIT license.');

        Console::WriteLine("\n{!f(yellow)}Applied options{/f}:");
        foreach ($this->_Options as $key => $value)
            Console::WriteLine("\t{!f(light_green)}".$key.'{/f}: '.$value);

        Console::WriteLine("\n{!f(yellow)}Working Path{/f}:\n\t".WEAVER_WORKING_PATH);
        Console::WriteLine("\n{!f(yellow)}Temporary Path{/f}:\n\t".$this->GetSelifaTemporaryPath());
        Console::WriteLine("\n{!f(yellow)}Selifa Application Path{/f}:\n\t".$this->GetSelifaApplicationPath());
        Console::WriteLine("\n{!f(yellow)}Weaver User Path{/f}:\n\t".$this->GetOption('UserPath',''));

        Console::WriteLine("\n{!f(yellow)}Weaver applications{/f}:");
        $apps = $this->GetAllApplicationObjects(false);
        foreach ($apps as $key => $ao)
            Console::WriteLine("\t{!f(light_green)}".$key.'{/f}: '.$ao->GetShortDescription());

        Console::WriteLine("\n{!f(yellow)}User applications{/f}:");
        $apps = $this->GetAllApplicationObjects(true);
        foreach ($apps as $key => $ao)
            Console::WriteLine("\t{!f(light_green)}".$key.'{/f}: '.$ao->GetShortDescription());

        Console::WriteLine('');
    }

    /**
     * @throws Exception
     */
    public function Start()
    {
        $argument = CommandLineArgument::Initialize();
        if ($argument->Count > 1)
        {
            $appId = $argument->ApplicationID;
            if ($appId != '')
            {
                if ($this->_Options['DefaultNamespace'] != '')
                    $className = ($this->_Options['DefaultNamespace']."\\".$appId.$this->_Options['ClassSuffix']);
                else
                    $className = ($appId.$this->_Options['ClassSuffix']);

                $isUserApp = false;
                $bFileName = ($this->GetApplicationPath(false,true).$appId.'.php');
                if (!file_exists($bFileName))
                {
                    $uFileName = ($this->GetApplicationPath(true,true).$appId.'.php');
                    if (!file_exists($uFileName))
                        throw new Exception('Application file "'.$appId.'" does not exists.');
                    else
                    {
                        require_once($uFileName);
                        $isUserApp = true;
                    }

                }
                else
                    require_once($bFileName);

                $appObject = new $className();
                if (!($appObject instanceof BaseApplication))
                    throw new Exception('Class "'.$className.'" is an instance of BaseApplication.');

                $appObject->IsUserApplication = $isUserApp;
                $appObject->Initialize($this,$argument->Options);
                $appObject->Run($this,$argument->Command,$argument->Parameters);
                $appObject->Finalize($this);
            }
            else
                $this->OutputAbout();
        }
        else
            $this->OutputAbout();
    }

    /**
     * @param bool $isUser
     * @return BaseApplication[]
     */
    public function GetAllApplicationObjects($isUser=false)
    {
        $result = array();
        $aDir = $this->GetApplicationPath($isUser,false);

        if (!file_exists($aDir))
            return array();

        $files = array();
        Directory::Enumerate($aDir,'',$files);
        foreach ($files as $file)
        {
            $dix = strrpos($file,'.');
            if ($dix !== false)
                $aKey = trim(substr($file,0,$dix));
            else
                $aKey = trim($file);
            $fullPath = ($aDir.DIRECTORY_SEPARATOR.$file);

            if (!file_exists($fullPath))
                continue;

            $className = (str_replace('/',"\\",$aKey).$this->_Options['ClassSuffix']);
            if ($this->_Options['DefaultNamespace'] != '')
                $className = ($this->_Options['DefaultNamespace']."\\".$className);

            if (!class_exists($className,false))
                include_once($fullPath);

            $appObject = new $className();
            if (!($appObject instanceof BaseApplication))
                continue;

            $result[$aKey] = $appObject;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function GetWeaverComposeData()
    {
        if (isset($this->ComposeData['weaver']))
            return $this->ComposeData['weaver'];
        else
            return array();
    }

    /**
     *
     */
    public function WriteComposeData()
    {
        if ($this->_SelifaComposeFile != '')
        {
            $content = json_encode($this->ComposeData,JSON_PRETTY_PRINT);
            file_put_contents($this->_SelifaComposeFile,$content);
        }
    }

    /**
     * @return array
     */
    public function GetComponentConfigurations()
    {
        if ($this->InstallData == null)
            return array();

        $config = array();
        foreach ($this->InstallData['packages'] as $pName => $meta)
        {
            $pConfig = $meta['configurations'];
            if (count($pConfig) > 0)
            {
                foreach ($pConfig as $key => $options)
                    $config[$key] = $options;
            }
        }
        return $config;
    }

    #region Selifa Application
    /**
     * @param string $appKey
     * @param bool $createDir
     * @param bool $fullPath
     * @return string
     */
    public function GetSelifaApplicationPath($appKey='',$createDir=false,$fullPath=true)
    {
        $appPath = 'apps';
        if (isset($this->ComposeData['app-dir']))
            $appPath = $this->ComposeData['app-dir'];

        if ($appKey != '')
            $dir = ($appPath.DIRECTORY_SEPARATOR.$appKey);
        else
            $dir = ($appPath);

        if ($fullPath)
            $dir = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR.$dir);
        if ($createDir)
        {
            if (!file_exists($dir))
                mkdir($dir,0775,true);
        }
        return $dir;
    }

    /**
     * @return string
     */
    public function GetSelifaLibraryPath()
    {
        $path = 'libraries';
        if (isset($this->ComposeData['lib-dir']))
            $path = $this->ComposeData['lib-dir'];
        return (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR.$path);
    }

    /**
     * @return string
     */
    public function GetSelifaTemporaryPath()
    {
        $tempPath = 'temps';
        if (isset($this->ComposeData['temp-dir']))
            $tempPath = $this->ComposeData['temp-dir'];
        $dir = (WEAVER_WORKING_PATH.DIRECTORY_SEPARATOR.$tempPath);
        if (!file_exists($dir))
            mkdir($dir,0775,true);
        return $dir;
    }

    /**
     * @param string $appKey
     * @param array $appData
     */
    public function SetApplicationData($appKey,$appData)
    {
        if (!isset($this->ComposeData['applications']))
            $this->ComposeData['applications'] = array(
                $appKey => $appData
            );
        else
        {
            if (count($this->ComposeData['applications']) <= 0)
                $this->ComposeData['applications'] = array(
                    $appKey => $appData
                );
            else
                $this->ComposeData['applications'][$appKey] = $appData;
        }
    }

    /**
     * @param string $appKey
     */
    public function RemoveApplicationData($appKey)
    {
        if (isset($this->ComposeData['applications'][$appKey]))
            unset($this->ComposeData['applications'][$appKey]);
    }
    #endregion
}
?>