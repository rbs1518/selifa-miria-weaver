<?php
namespace WeaverSQLReflector;
use RBS\Selifa\Data\BaseDatabaseDriver;
use RBS\Selifa\Miria\Weaver\SQL\BaseReflector;
use RBS\Utility\F;
use RBS\Utility\Inflector;

/**
 * Class mysqlReflector
 * @package WeaverSQLReflector
 */
class mysqlReflector extends BaseReflector
{
    /**
     * @param BaseDatabaseDriver $driver
     * @param array $ts
     * @return mixed
     */
    protected function _RetrieveTableMeta($driver, &$ts)
    {
        $allowedEngine = array('myisam','innodb','federated');
        $driver->Prepare('SHOW TABLE STATUS')->GetData()->Each(function($row) use($driver,&$ts,&$allowedEngine)
        {
            $engine = trim($row['Engine']);
            if (in_array(strtolower($engine),$allowedEngine))
            {
                $tblName = strtolower(trim($row['Name']));

                $sRows = array();
                $driver->Prepare('DESC '.$tblName)->GetData()->Each(function($row) use(&$sRows)
                {
                    $fieldName = trim($row['Field']);
                    $key = strtoupper(trim($row['Key']));

                    $metaKey = strtolower($fieldName);
                    $isMeta = isset($this->MetaColumns[$metaKey]);

                    $sRows[] = array(
                        'field' => $fieldName,
                        'type' => strtoupper(trim($row['Type'])),
                        'nullable' => (strtoupper(trim($row['Null'])) == 'YES'),
                        'primary' => ($key == 'PRI'),
                        'unique' => ($key == 'UNI'),
                        'multiple' => ($key == 'MUL'),
                        'default' => $row['Default'],
                        'extra' => strtoupper(trim($row['Extra'])),
                        'blob' => (stripos('BLOB',strtoupper(trim($row['Type']))) > -1),
                        'meta' => ($isMeta?$this->MetaColumns[$metaKey]:null),
                        'commit' => '',
                        //'group_id' => true, //if this column is a group identifier, then set true
                        //'group_name' => 'registration', //group name which this column belong to
                    );
                },false);
                $collations = explode('_',strtolower(trim($row['Collation'])));

                $sIndexes = array();
                $driver->Prepare('SHOW INDEX FROM '.$tblName)->GetData()->Each(function($row) use(&$sIndexes)
                {
                    $keyName = strtoupper(trim($row['Key_name']));
                    if ($keyName != 'PRIMARY')
                    {
                        if (isset($sIndexes[$keyName]))
                        {
                            $sIndexes[$keyName]['fields'][] = trim($row['Column_name']);
                        }
                        else
                        {
                            $sIndexes[$keyName] = array(
                                'name' => trim($row['Key_name']),
                                'unique' => (strtolower(trim($row['Non_unique'])) != '1'),
                                'type' => strtoupper(trim($row['Index_type'])),
                                'fields' => array(trim($row['Column_name']))
                            );
                        }
                    }
                },false);

                $ts[] = array(
                    'name' => trim($row['Name']),
                    'engine' => $engine,
                    'ai' => (int)trim($row['Auto_increment']),
                    'collation' => trim($row['Collation']),
                    'charset' => (count($collations)>0?$collations[0]:''),
                    'rows' => $sRows,
                    'indexes' => $sIndexes,
                    'commit' => '',
                );
            }
        },false);
    }

    /**
     * @param array $ts
     * @return string
     */
    protected function _CreateTableSyntax($ts)
    {
        $s  = "DROP TABLE IF EXISTS `".$ts['name']."`;\n/*!40101 SET @saved_cs_client     = @@character_set_client */;\n";
        $s .= "/*!40101 SET character_set_client = ".$ts['charset']." */;\nCREATE TABLE `".$ts['name']."` (\n";

        $primaries = array();
        $uniques = array();

        $rowStrings = array();
        foreach ($ts['rows'] as $row)
        {
            $isPrimary = (bool)F::Extract($row,'primary',false);
            if ($isPrimary)
                $primaries[] = ('`'.$row['field'].'`');
            $isUnique = (bool)F::Extract($row,'unique',false);
            if ($isUnique)
                $uniques[] = ('`'.$row['field'].'`');

            $f = ("\t`".$row['field'].'` '.$row['type'].(!$row['nullable']?' NOT NULL':''));

            $defaultValue = F::Extract($row,'default',null);
            if ($defaultValue !== null)
            {
                $defaultValue = trim($defaultValue);
                if (is_numeric($defaultValue))
                    $f .= (" DEFAULT ".$defaultValue);
                else
                    $f .= (" DEFAULT '".$defaultValue."'");
            }

            $extra = trim(F::Extract($row,'extra',''));
            if ($extra != '')
                $f .= (' '.$row['extra']);
            $rowStrings[] = $f;
        }

        if (count($primaries) > 0)
            $rowStrings[] = "\tPRIMARY KEY (".implode(',',$primaries).")";
        if (count($uniques) > 0)
            $rowStrings[] = "\tUNIQUE KEY (".implode(',',$uniques).")";

        $indexes = F::Extract($ts,'indexes',array());
        if (count($indexes) > 0)
        {
            foreach ($ts['indexes']  as $indexKey => $idx)
            {
                $rowStrings[] = ("\tINDEX `".$indexKey.'` USING '.$idx['type'].' (`'.implode('`,`',$idx['fields']).'`)');
            }
        }

        $ai = (int)F::Extract($ts,'ai',1);

        $s .= implode(",\n",$rowStrings);
        $s .= "\n) ENGINE=".$ts['engine']." AUTO_INCREMENT=".$ai." DEFAULT CHARSET=".$ts['charset'].";\n/*!40101 SET character_set_client = @saved_cs_client */;";
        return $s;
    }

    /**
     * @param array $ts
     * @param bool $updateLog
     * @param bool $changeLog
     * @return string
     */
    protected function _CreateAfterInsertTrigger($ts, $updateLog = false, $changeLog = false)
    {
        $doNotLog = (isset($ts['do_not_log'])?$ts['do_not_log']:false);
        if ($doNotLog)
            return '';

        $s  = "DROP TRIGGER /*!50030 IF EXISTS */ `Trigger_".$ts['name']."_AfterInsert`;\nDELIMITER $$\n";
        $s .= "CREATE TRIGGER `Trigger_".$ts['name']."_AfterInsert` AFTER INSERT ON `".$ts['name']."` FOR EACH ROW\nBEGIN\n";
        if (isset($ts['triggers']['after_insert']['before_log']))
            $s .= $ts['triggers']['after_insert']['before_log'];
        if ($updateLog)
        {
            $s .= "\t-- Insert into UpdateLog table.\n";
            $s .= "\tINSERT INTO `UpdateLog`(`UID`,`DBU`,`TableName`,`RowID`,`EventType`,`Data`,`Remark`)\n";
            $s .= "\t\tVALUES (NEW.CUID,CURRENT_USER,'".$ts['name']."',NEW.ID,'INSERT','','');\n";
        }
        if ($changeLog)
        {
            foreach ($ts['rows'] as $row)
            {
                $hasGroupId = (bool)F::Extract($row,'group_id',false);
                $groupName = trim(F::Extract($row,'group_name',''));
                if ($hasGroupId && ($groupName != ''))
                {
                    $s .= "\n";
                    $cltName = (Inflector::camelize($groupName).'ChangeLog');
                    $s .= "\t-- Insert into ".$cltName." table.\n";
                    $s .= "\tINSERT INTO `".$cltName."`(`UID`,`DBU`,`TableName`,`RowID`,`Identifier`,`EventType`)\n";
                    $s .= "\t\tVALUES (NEW.CUID,CURRENT_USER,'".$ts['name']."',NEW.ID,NEW.".$row['field'].",'INSERT');\n";
                }
            }
        }
        if (isset($ts['triggers']['after_insert']['after_log']))
            $s .= $ts['triggers']['after_insert']['after_log'];
        $s .= "END $$\nDELIMITER ;\n";
        return $s;
    }

    /**
     * @param array $ts
     * @param bool $updateLog
     * @param bool $changeLog
     * @return string
     */
    protected function _CreateAfterUpdateTrigger($ts, $updateLog = false, $changeLog = false)
    {
        $doNotLog = (isset($ts['do_not_log'])?$ts['do_not_log']:false);
        if ($doNotLog)
            return '';

        $s  = "DROP TRIGGER /*!50030 IF EXISTS */ `Trigger_".$ts['name']."_AfterUpdate`;\nDELIMITER $$\n";
        $s .= "CREATE TRIGGER `Trigger_".$ts['name']."_AfterUpdate` AFTER UPDATE ON `".$ts['name']."` FOR EACH ROW\nBEGIN\n";
        if (isset($ts['triggers']['after_update']['before_log']))
            $s .= $ts['triggers']['after_update']['before_log'];
        if ($updateLog)
        {
            $s .= "\t-- Insert into UpdateLog table.\n";
            $s .= "\tDECLARE _Temp TEXT;\n";
            $s .= "\tSET _Temp = '';\n";

            foreach ($ts['rows'] as $row)
            {
                $meta = F::Extract($row,'meta');

                $skipLog = false;
                if ($meta !== null)
                {
                    $skipLog = (bool)F::Extract($meta,'skip_log',false);
                }

                if (!$skipLog)
                {
                    $s .= "\tIF (NEW.".$row['field']." <> OLD.".$row['field'].") THEN\n";
                    $s .= "\t\tSET _Temp = CONCAT(_Temp,'".$row['field']." : ',OLD.".$row['field'].",' : ',NEW.".$row['field'].",'|\\r\\n|');\n";
                    $s .= "\tEND IF;\n";
                }
            }

            $s .= "\tINSERT INTO `UpdateLog`(`UID`,`DBU`,`TableName`,`RowID`,`EventType`,`Data`,`Remark`)\n";
            $s .= "\t\tVALUES (NEW.CUID,CURRENT_USER,'".$ts['name']."',NEW.ID,'UPDATE',_Temp,'');\n\n";
        }
        if ($changeLog)
        {
            $clCount = 0;
            $cls = '';
            foreach ($ts['rows'] as $row)
            {
                $hasGroupId = (bool)F::Extract($row,'group_id',false);
                $groupName = trim(F::Extract($row,'group_name',''));
                if ($hasGroupId && ($groupName != ''))
                {
                    $cltName = (Inflector::camelize($groupName).'ChangeLog');
                    $cls .= "\t\t-- Insert into ".$cltName." table.\n";
                    $cls .= "\t\tINSERT INTO `".$cltName."`(`UID`,`DBU`,`TableName`,`RowID`,`Identifier`,`EventType`)\n";
                    $cls .= "\t\t\tVALUES (NEW.CUID,CURRENT_USER,'".$ts['name']."',NEW.ID,NEW.".$row['field'].",'UPDATE');\n\n";
                    $clCount++;
                }
            }

            if ($clCount > 0)
                $s .= ("\tIF (_Temp <> '') THEN\n".$cls."\tEND IF;\n");
        }
        if (isset($ts['triggers']['after_update']['after_log']))
            $s .= $ts['triggers']['after_update']['after_log'];
        $s .= "END $$\nDELIMITER ;\n";
        return $s;
    }

    /**
     * @param array $ts
     * @param bool $updateLog
     * @param bool $changeLog
     * @return string
     */
    protected function _CreateAfterDeleteTrigger($ts, $updateLog = false, $changeLog = false)
    {
        $doNotLog = (isset($ts['do_not_log'])?$ts['do_not_log']:false);
        if ($doNotLog)
            return '';

        $s  = "DROP TRIGGER /*!50030 IF EXISTS */ `Trigger_".$ts['name']."_AfterDelete`;\nDELIMITER $$\n";
        $s .= "CREATE TRIGGER `Trigger_".$ts['name']."_AfterDelete` AFTER DELETE ON `".$ts['name']."` FOR EACH ROW\nBEGIN\n";
        if (isset($ts['triggers']['after_delete']['before_log']))
            $s .= $ts['triggers']['after_delete']['before_log'];
        if ($updateLog)
        {
            $s .= "\t-- Insert into UpdateLog table.\n";
            $s .= "\tINSERT INTO `UpdateLog`(`UID`,`DBU`,`TableName`,`RowID`,`EventType`,`Data`,`Remark`)\n";
            $s .= "\t\tVALUES (0,CURRENT_USER,'".$ts['name']."',OLD.ID,'DELETE','','');\n";
        }
        if ($changeLog)
        {
            foreach ($ts['rows'] as $row)
            {
                $hasGroupId = (bool)F::Extract($row,'group_id',false);
                $groupName = trim(F::Extract($row,'group_name',''));
                if ($hasGroupId && ($groupName != ''))
                {
                    $s .= "\n";
                    $cltName = (Inflector::camelize($groupName).'ChangeLog');
                    $s .= "\t-- Insert into ".$cltName." table.\n";
                    $s .= "\tINSERT INTO `".$cltName."`(`UID`,`DBU`,`TableName`,`RowID`,`Identifier`,`EventType`)\n";
                    $s .= "\t\tVALUES (0,CURRENT_USER,'".$ts['name']."',OLD.ID,OLD.".$row['field'].",'DELETE');\n";
                }
            }
        }
        if (isset($ts['triggers']['after_delete']['after_log']))
            $s .= $ts['triggers']['after_delete']['after_log'];
        $s .= "END $$\nDELIMITER ;\n";
        return $s;
    }
}
?>