<?php
define('SELIFA_TIME_STARTED',microtime(true));
define('SELIFA','v0.2');
define('SELIFA_NAME','Selifa Miria Weaver');

$workPath = WEAVER_WORKING_PATH;
$sComposeFile = ($workPath.DIRECTORY_SEPARATOR.'.selifa-compose.json');
if (!file_exists($sComposeFile))
    throw new Exception('Selifa compose file is missing.');

$cData = json_decode(file_get_contents($sComposeFile),true);
if (!isset($cData['weaver']))
    $cData['weaver'] = array(
        'scaffolding-path' => 'scaffolds',
        'config-path' => 'configs',
        'configs' => array(),
        'generators' => array(
            'application' => array(
                'default' => "RBS\\Selifa\\Miria\\Weaver\\Generator\\defaultAppGenerator",
                'rpc' => "RBS\\Selifa\\Miria\\Weaver\\Generator\\rpcAppGenerator"
            )
        )
    );

$scfPath = $cData['weaver']['scaffolding-path'];
if (!file_exists($workPath.DIRECTORY_SEPARATOR.$scfPath))
    mkdir($workPath.DIRECTORY_SEPARATOR.$scfPath,0777,true);

$cfgPath =  $cData['weaver']['config-path'];
$wConfigDir = ($scfPath.DIRECTORY_SEPARATOR.$cfgPath.DIRECTORY_SEPARATOR);

include($workPath.DIRECTORY_SEPARATOR.'selifa/core/Core.php');
\RBS\Selifa\Core::Initialize(array(
    'RootPath' => $workPath,
    'ConfigDir' => $wConfigDir,
    'LoadComponents' => array(
    '\RBS\Selifa\XM'
)
));

$weaver = \RBS\Selifa\Miria\Weaver::Initialize(array());
$weaver->SetComposeData($cData,$sComposeFile);

$sInstallFile = ($workPath.DIRECTORY_SEPARATOR.'.selifa-install.json');
if (file_exists($sInstallFile))
{
    $iData = json_decode(file_get_contents($sInstallFile),true);
    $weaver->InstallData = $iData;
}

$weaver->Start();
?>