<?php
namespace WeaverApplication;
use RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver\DBEnabledApplication;
use RBS\Selifa\Miria\Weaver\IO\Console;
use RBS\Selifa\Miria\Weaver\IO\File;
use Exception;

/**
 * Class modelApplication
 * @package WeaverApplication
 */
class modelApplication extends DBEnabledApplication
{
    /**
     * @param Weaver $weaver
     * @param array $options
     */
    public function Initialize(Weaver $weaver, $options)
    {
        parent::Initialize($weaver,$options);
        $this->Data = $weaver->ReadScaffoldingJSON('model.'.$this->DatabaseId);

        if (!isset($this->Data['tables']))
            $this->Data['tables'] = array();
        $this->Reflector->SetTables($this->Data['tables']);
    }

    /**
     * @param Weaver $weaver
     */
    public function Finalize(Weaver $weaver)
    {
        $this->Data['tables'] = $this->Reflector->GetTables();
        $weaver->WriteScaffoldingJSON('model.'.$this->DatabaseId,$this->Data);
    }

    /**
     * @return string
     */
    public function GetShortDescription()
    {
        return 'Database model utilities and schema refactoring.';
    }

    /**
     * @param Weaver $weaver
     * @param string $command
     * @param array $parameters
     */
    public function Run(Weaver $weaver, $command, $parameters)
    {
        if ($command == 'reflect')
        {
            $this->Reflector->ReverseEngineerSchema();
            Console::WriteLine('{!f(light_green)}Database schema reflected.{/f}');
        }
        else if ($command == 'reset-ai')
        {
            $this->Reflector->ResetAutoIncrementForAllTables();
            Console::WriteLine('{!f(light_green)}Auto increment on all table resetted.{/f}');
        }
        else if ($command == 'add-update-log')
        {
            $this->Reflector->CreateUpdateLogTableSpec();
        }
        else if ($command == 'generate-schema')
        {
            $tableName = '';
            if (isset($parameters[0]))
                $tableName = trim($parameters[0]);

            if ($tableName != '')
            {
                $content = $this->Reflector->CreateTableSyntax($tableName);
                if ($content != '')
                {
                    $fileName = ($weaver->GetSelifaTemporaryPath().DIRECTORY_SEPARATOR.'sql.schema.'.strtolower($this->DatabaseId).'.'.$tableName.'.sql');
                    File::Put($fileName,$content,true);
                    Console::WriteLine('Schema for table {!f(light_green)}"'.$tableName.'"{/f} generated.');
                }
                else
                    Console::WriteLine('Table {!f(light_red)}"'.$tableName.'"{/f} not found.');
            }
            else
            {
                $content = $this->Reflector->CreateTableSyntax();
                $fileName = ($weaver->GetSelifaTemporaryPath().DIRECTORY_SEPARATOR.'sql.schema.'.strtolower($this->DatabaseId).'.sql');
                File::Put($fileName,$content,true);
                Console::WriteLine('Table schema for database {!f(light_green)}"'.$this->DatabaseId.'"{/f} generated.');
            }
        }
        else if ($command == 'generate-triggers')
        {
            $tableName = '';
            if (isset($parameters[0]))
                $tableName = trim($parameters[0]);

            if ($tableName != '')
            {
                $content = $this->Reflector->CreateTableTriggerSyntax($tableName,true,true);
                if ($content != '')
                {
                    $fileName = ($weaver->GetSelifaTemporaryPath().DIRECTORY_SEPARATOR.'sql.trigger.'.strtolower($this->DatabaseId).'.'.$tableName.'.sql');
                    File::Put($fileName,$content,true);
                    Console::WriteLine('Trigger for table {!f(light_green)}"'.$tableName.'"{/f} generated.');
                }
                else
                    Console::WriteLine('Table {!f(light_red)}"'.$tableName.'"{/f} not found.');
            }
            else
            {
                $content = $this->Reflector->CreateTableTriggerSyntax('',true,true);
                $fileName = ($weaver->GetSelifaTemporaryPath().DIRECTORY_SEPARATOR.'sql.trigger.'.strtolower($this->DatabaseId).'.sql');
                File::Put($fileName,$content,true);
                Console::WriteLine('Trigger schema for database {!f(light_green)}"'.$this->DatabaseId.'"{/f} generated.');
            }
        }
    }
}
?>