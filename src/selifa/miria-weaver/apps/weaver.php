<?php
namespace WeaverApplication;
use RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver\BaseApplication;
use RBS\Selifa\Miria\Weaver\IO\Console;
use RBS\Selifa\Miria\Weaver\Generator;
use RBS\Selifa\Miria\Weaver\IO\File;
use Exception;

/**
 * Class weaverApplication
 * @package WeaverApplication
 */
class weaverApplication extends BaseApplication
{
    /**
     * @param Weaver $weaver
     * @param array $options
     */
    public function Initialize(Weaver $weaver, $options)
    {

    }

    /**
     * @return string
     */
    public function GetShortDescription()
    {
        return 'Miria Weaver configurator and utilities.';
    }

    /**
     * @param Weaver $weaver
     * @param string $command
     * @param array $parameters
     * @throws Exception
     */
    public function Run(Weaver $weaver, $command, $parameters)
    {
        if ($command == 'init')
        {

        }
        else if ($command == 'update-config')
        {
            $cfgPath = $weaver->GetConfigPath();
            $configs = $weaver->GetComponentConfigurations();

            $cfgOverrides = array();
            if (isset($weaver->ComposeData['weaver']['configs']))
                $cfgOverrides = $weaver->ComposeData['weaver']['configs'];

            $gen = new Generator();
            foreach ($configs as $cKey => $options)
            {
                $aOptions = $options;
                if (array_key_exists($cKey,$cfgOverrides))
                {
                    if ($cfgOverrides[$cKey] != null)
                        $aOptions = array_replace_recursive($aOptions,$cfgOverrides[$cKey]);
                    else
                        $aOptions = null;
                }

                if ($aOptions == null)
                    continue;

                $filePath = ($cfgPath.$cKey.'.php');
                $fileContent = $gen->CreateConfigurationFile($aOptions);
                File::Put($filePath,$fileContent,true);
                Console::WriteLine("\tUpdating configuration {!f(light_cyan)}".$cKey."{/f}");
            }
        }
    }
}
?>