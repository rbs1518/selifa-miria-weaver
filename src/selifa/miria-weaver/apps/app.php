<?php
namespace WeaverApplication;
use RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver\BaseApplication;
use RBS\Selifa\Miria\Weaver\Command\ApplicationCommandRunner;
use RBS\Selifa\Miria\Weaver\SelifaApplicationScaffolding;
use RBS\Selifa\Miria\Weaver\IO\Console;
use RBS\Selifa\Miria\Weaver\IO\ConsoleFormatParser;
use Exception;

/**
 * Class appApplication
 * @package WeaverApplication
 */
class appApplication extends BaseApplication
{
    /**
     * @var bool
     */
    protected $DisableSave = false;

    /**
     * @var string
     */
    protected $AppType = 'default';

    /**
     * @var bool
     */
    protected $Quiet = false;

    /**
     * @var bool
     */
    protected $NoConfirm = false;

    /**
     * @var bool
     */
    protected $ForceReInit = false;

    /**
     * @var array
     */
    protected $Options = array();

    /**
     * @var array
     */
    protected $_InitTemps = array(
        'name' => 'Web Application',
        'type' => 'default',
        'rootns' => 'WebApplication'
    );

    /**
     * @param Weaver $weaver
     * @param array $options
     */
    public function Initialize(Weaver $weaver, $options)
    {
        if (isset($options['disable-save']))
            $this->DisableSave = $options['disable-save'];
        if (isset($options['type']))
            $this->AppType = strtolower(trim($options['type']));
        if (isset($options['quiet']))
            $this->Quiet = true;
        if (isset($options['no-confirm']))
            $this->NoConfirm = true;
        if (isset($options['re-init']))
            $this->ForceReInit = true;

        $this->Options = $options;
    }

    /**
     * @return string
     */
    public function GetShortDescription()
    {
        return 'Selifa application configurator and scaffolding.';
    }

    /**
     * @param Weaver $weaver
     * @param string $command
     * @param array $parameters
     * @throws Exception
     */
    public function Run(Weaver $weaver, $command, $parameters)
    {
        if ($command == 'create')
        {
            if ($this->Quiet)
            {
                if (!isset($parameters[0]))
                    throw new Exception('Selifa application is not specified.');
                $appKey = strtolower(trim($parameters[0]));
                if (isset($this->Options['name']))
                    $appName = trim($this->Options['name']);
                else
                    $appName = $this->_InitTemps['name'];
                if (isset($this->Options['type']))
                    $this->AppType = strtolower(trim($this->Options['type']));
                else
                    $this->AppType = $this->_InitTemps['type'];
                if (isset($this->Options['ns']))
                    $rootNamespace = trim($this->Options['ns']);
                else
                    $rootNamespace = $this->_InitTemps['rootns'];
            }
            else
            {
                if (isset($parameters[0]))
                    $appKey = strtolower(trim($parameters[0]));
                else
                    $appKey = Console::AskForValue('Application {!f(light_green)}id{/f}/{!f(light_green)}key{/f}: ',array(),true);
                if (isset($this->Options['name']))
                    $appName = trim($this->Options['name']);
                else
                    $appName = Console::AskForValueWithDefault('Application {!f(light_green)}name{/f}:','Web Application');
                if (isset($this->Options['type']))
                    $this->AppType = strtolower(trim($this->Options['type']));
                else
                    $this->AppType = Console::AskForValueWithDefault('Application {!f(light_green)}type{/f}:','default');
                if (isset($this->Options['ns']))
                    $rootNamespace = trim($this->Options['ns']);
                else
                    $rootNamespace = Console::AskForValueWithDefault('Root namespace:','WebApplication');
            }

            $sas = new SelifaApplicationScaffolding($weaver,$appKey,$this->DisableSave);
            $aParams = array(
                'Key' => $appKey,
                'Type' => $this->AppType,
                'Name' => $appName,
                'RootNS' => $rootNamespace
            );

            $appGen = $sas->LoadApplicationGenerator($this->AppType);
            $appGen->GetCustomParameter($weaver,$sas,$this->Options,$aParams,$this->Quiet);

            $sas->InitializeApplication($aParams,$appGen,$this->NoConfirm,$this->ForceReInit);
        }
        else
        {
            if (!isset($parameters[0]))
                throw new Exception('Selifa application is not specified.');

            $cr = new ApplicationCommandRunner($weaver,$this,$command);
            $cr->SetApplication(strtolower(trim($parameters[0])),$this->DisableSave);
            $cr->Execute($parameters);
        }
    }
}
?>