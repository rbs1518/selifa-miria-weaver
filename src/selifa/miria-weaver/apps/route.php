<?php
namespace WeaverApplication;
use RBS\Selifa\Miria\Weaver;
use RBS\Selifa\Miria\Weaver\BaseApplication;
use RBS\Selifa\Miria\Weaver\IO\Console;
use RBS\Selifa\Miria\Weaver\SelifaApplicationScaffolding;
use Exception;

/**
 * Class routeApplication
 * @package WeaverApplication
 */
class routeApplication extends BaseApplication
{
    /**
     * @var bool
     */
    protected $ShowList = false;

    /**
     * @var bool
     */
    protected $ShowJSON = false;

    /**
     * @var bool
     */
    protected $DisableSave = false;

    /**
     * @param Weaver $weaver
     * @param array $options
     */
    public function Initialize(Weaver $weaver, $options)
    {
        if (isset($options['list']))
            $this->ShowList = $options['list'];
        if (isset($options['json']))
            $this->ShowJSON = $options['json'];
        if (isset($options['disable-save']))
            $this->DisableSave = $options['disable-save'];

    }

    /**
     * @return string
     */
    public function GetShortDescription()
    {
        return "Application's route management and utilities.";
    }

    /**
     * @param Weaver $weaver
     * @param string $command
     * @param array $parameters
     * @throws Exception
     */
    public function Run(Weaver $weaver, $command, $parameters)
    {
        if (!isset($parameters[0]))
            throw new Exception('Selifa application is not specified.');

        $appKey = strtolower(trim($parameters[0]));
        $appPath = $weaver->GetSelifaApplicationPath($appKey);
        $sas = new SelifaApplicationScaffolding($weaver,$appKey,$this->DisableSave);

        if ($command == 'add')
        {
            if (!isset($parameters[1]))
                throw new Exception('Route key is not specified.');
            if (!isset($parameters[2]))
                throw new Exception('Route target is not specified.');

            $method = '';
            if (isset($parameters[3]))
                $method = strtoupper(trim($parameters[3]));

            $sas->Route->Add($parameters[1],$parameters[2],$method);
            Console::WriteLine('{!f(light_green)}Route added.{/f}');

            if ($this->ShowList)
                $sas->Route->WriteList();

            $sas->Route->GenerateFile($appPath);
        }
        else if ($command == 'list')
        {
            if (isset($parameters[1]))
            {
                $baseKey = trim($parameters[1]);
                $sas->Route->WriteList($baseKey);
            }
            else
                $sas->Route->WriteList();
        }
        else if ($command == 'import-include')
        {
            if (!isset($parameters[1]))
                throw new Exception('Filename is not specified.');

            $fileName = trim($parameters[1]);
            if (!file_exists($fileName))
                throw new Exception('File '.$fileName.' does not exists.');

            $content = include($fileName);
            foreach ($content as $key => $item)
            {
                if (is_array($item))
                {
                    foreach ($item as $method => $target)
                        $sas->Route->Add($key,$target,$method);
                }
                else
                {
                    $target = trim($item);
                    $sas->Route->Add($key,$target);
                }
            }

            if ($this->ShowList)
                $sas->Route->WriteList();

            $sas->Route->GenerateFile($appPath);
        }
        else if ($command == 'generate')
        {
            $sas->Route->GenerateFile($appPath);
            Console::WriteLine('{!f(light_green)}Route file generated.{/f}');
        }
        else if ($command == 'update')
        {
            
        }
        else if ($command == 'remove')
        {

        }
        /*else if ($command == 'test')
        {
            $xFile = "contoh.txt";
            $content = file_get_contents($xFile);
            Console::WriteLine($content);
        }*/

        if ($this->ShowJSON)
        {
            $routes = $sas->Route->GetRouteDataAsArray();
            echo json_encode($routes,JSON_PRETTY_PRINT)."\n";
        }
    }
}
?>