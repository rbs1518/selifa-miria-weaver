# Selifa Miria Weaver #

### Introduction ###
Selifa Miria Weaver is a command line application framework based on Selifa framework originally for Miria-based application scaffolding. Can be extended for custom command line utilities.

### Usage Instructions ###

##### Create Miria application (interactive) 
```text
miria app create
```

##### Creat Miria application (quiet)
```text
miria app create <app-key> --quiet    
```

Specify options to change default values
1. `--name="<app-name>"` to change application's name.
2. `--type="<app-type>"` to change application's type. Currently available are `default` and `rpc`.
3. `--ns="<root-namespace>"` to change default namespace for controller classes.
4. `--create-bc` to enable custom base controller class.
5. `--bc-name="<class-name>"` to change default base controller class name, if `--create-bc` is specified.
6. `--no-confirm` to disable final parameter confirmation.
7. `--re-init` to force re-initialization existing application.

 
##### Update Miria application configuration
```text
miria app update <app-key>
```

### Credits ###
Rinardi B. Sarean, rinardi_1518_sarean@hotmail.com.

## License ###
The MIT License (MIT)

Copyright (c) 2015-2017. Rinardi B. Sarean

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.